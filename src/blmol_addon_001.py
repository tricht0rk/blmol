bl_info = {
    "name": "Blender for Molecules",
    "blender": (2, 80, 0),
    "category": "Add Mesh",
}

import numpy as np, time
import random
#from scipy.misc import toimage
from scipy.spatial import distance_matrix
from scipy.interpolate import RegularGridInterpolator
from scipy.special import sph_harm
from math import isclose
import math
from mathutils import Matrix, Vector
import copy

# Python outside of Blender doesn't play all that well with bpy, so need
# to handle ImportError.
try:
    import bpy
    import bmesh
    bpy_avail = True
    from bpy.props import (StringProperty,
                       BoolProperty,
                       IntProperty,
                       FloatProperty,
                       FloatVectorProperty,
                       EnumProperty,
                       PointerProperty,
                       )
    from bpy.types import (Panel,
                       Menu,
                       Operator,
                       PropertyGroup,
                       )

except ImportError:
    bpy_avail = False

try:
    from PIL import Image as pimg
    pil_avail = True
except ImportError:
    pil_avail = False

try:
    import mcubes
    mcubes_avail = True
except ImportError:
    mcubes_avail = False
    print("Could not load mcubes. No cube file support!")

#Ang2B = 1.889725989

# sources for colors and radii:
# https://en.wikipedia.org/wiki/CPK_coloring
# https://en.wikipedia.org/wiki/Atomic_radii_of_the_elements_(data_page)


# Dictionary of color definitions (RGB tuples). Blender RGB colors can
# be conveniently determined by using a uniform png of the desired color
# as a background image, then using the eyedropper. Colors with the
# "_cb" tag are based on colorblind-safe colors as described in this
# Nature Methods editorial DOI:10.1038/nmeth.1618.
COLORS = {
    'black': (0, 0, 0, 1),
    'dark_red': (0.55, 0, 0, 1),
    'gray': (0.2, 0.2, 0.2, 1),
    'dark_gray': (0.1, 0.1, 0.1, 1),
    'green': (0.133, 0.545, 0.133, 1),
    'green_surface': (0.133, 0.545, 0.133, 0.3),
    'red_surface': (0.8, 0., 0., 0.3),
    'blue_surface': (0., 0., 0.8, 0.3),
    'dark_green': (0.1, 0.5, 0.1, 1),
    'indigo': (0.294, 0, 0.509, 1),
    'light_gray': (0.7,0.7,0.7, 1),
    'orange': (1.0, 0.647, 0, 1),
    'purple': (0.627, 0.125, 0.941, 1),
    'red': (0.8, 0, 0, 1),
    'royal_blue': (0.255, 0.412, 0.882, 1),
    'white': (1.0, 1.0, 1.0, 1),
    'yellow': (1.0, 1.0, 0, 1),
    'blue_cb': (0, 0.168, 0.445, 1),
    'bluish_green_cb': (0, 0.620, 0.451, 1),
    'orange_cb': (0.791, 0.347, 0, 1),
    'reddish_purple_cb': (0.800, 0.475, 0.655, 1),
    'sky_blue_cb': (0.337, 0.706, 0.914, 1),
    'vermillion_cb': (0.665, 0.112, 0, 1),
    'yellow_cb': (0.871, 0.776, 0.054, 1),
    'cadmium_yellow': (248./255, 235./255, 0., 1),
    ### colors for elements of PSE ###
    'H': ( 1.000,  1.000,  1.000,  1.000),
    'HE': ( 0.851,  1.000,  1.000,  1.000),
    'LI': ( 0.800,  0.502,  1.000,  1.000),
    'BE': ( 0.761,  1.000,  0.000,  1.000),
    'B': ( 1.000,  0.710,  0.710,  1.000),
    'C': ( 0.565,  0.565,  0.565,  1.000),
    'N': ( 0.188,  0.314,  0.973,  1.000),
    'O': ( 1.000,  0.051,  0.051,  1.000),
    'F': ( 0.565,  0.878,  0.314,  1.000),
    'NE': ( 0.702,  0.890,  0.961,  1.000),
    'NA': ( 0.671,  0.361,  0.949,  1.000),
    'MG': ( 0.541,  1.000,  0.000,  1.000),
    'AL': ( 0.749,  0.651,  0.651,  1.000),
    'SI': ( 0.941,  0.784,  0.627,  1.000),
    'P': ( 1.000,  0.502,  0.000,  1.000),
    'S': ( 1.000,  1.000,  0.188,  1.000),
    'CL': ( 0.122,  0.941,  0.122,  1.000),
    'AR': ( 0.502,  0.820,  0.890,  1.000),
    'K': ( 0.561,  0.251,  0.831,  1.000),
    'CA': ( 0.239,  1.000,  0.000,  1.000),
    'SC': ( 0.902,  0.902,  0.902,  1.000),
    'TI': ( 0.749,  0.761,  0.780,  1.000),
    'V': ( 0.651,  0.651,  0.671,  1.000),
    'CR': ( 0.541,  0.600,  0.780,  1.000),
    'MN': ( 0.612,  0.478,  0.780,  1.000),
    'FE': ( 0.878,  0.400,  0.200,  1.000),
    'CO': ( 0.941,  0.565,  0.627,  1.000),
    'NI': ( 0.314,  0.816,  0.314,  1.000),
    'CU': ( 0.784,  0.502,  0.200,  1.000),
    'ZN': ( 0.490,  0.502,  0.690,  1.000),
    'GA': ( 0.761,  0.561,  0.561,  1.000),
    'GE': ( 0.400,  0.561,  0.561,  1.000),
    'AS': ( 0.741,  0.502,  0.890,  1.000),
    'SE': ( 1.000,  0.631,  0.000,  1.000),
    'BR': ( 0.651,  0.161,  0.161,  1.000),
    'KR': ( 0.361,  0.722,  0.820,  1.000),
    'RB': ( 0.439,  0.180,  0.690,  1.000),
    'SR': ( 0.000,  1.000,  0.000,  1.000),
    'Y': ( 0.580,  1.000,  1.000,  1.000),
    'ZR': ( 0.580,  0.878,  0.878,  1.000),
    'NB': ( 0.451,  0.761,  0.788,  1.000),
    'MO': ( 0.329,  0.710,  0.710,  1.000),
    'TC': ( 0.231,  0.620,  0.620,  1.000),
    'RU': ( 0.141,  0.561,  0.561,  1.000),
    'RH': ( 0.039,  0.490,  0.549,  1.000),
    'PD': ( 0.000,  0.412,  0.522,  1.000),
    'AG': ( 0.753,  0.753,  0.753,  1.000),
    'CD': ( 1.000,  0.851,  0.561,  1.000),
    'IN': ( 0.651,  0.459,  0.451,  1.000),
    'SN': ( 0.400,  0.502,  0.502,  1.000),
    'SB': ( 0.620,  0.388,  0.710,  1.000),
    'TE': ( 0.831,  0.478,  0.000,  1.000),
    'I': ( 0.580,  0.000,  0.580,  1.000),
    'XE': ( 0.259,  0.620,  0.690,  1.000),
    'CS': ( 0.341,  0.090,  0.561,  1.000),
    'BA': ( 0.000,  0.788,  0.000,  1.000),
    'LA': ( 0.439,  0.831,  1.000,  1.000),
    'CE': ( 1.000,  1.000,  0.780,  1.000),
    'PR': ( 0.851,  1.000,  0.780,  1.000),
    'ND': ( 0.780,  1.000,  0.780,  1.000),
    'PM': ( 0.639,  1.000,  0.780,  1.000),
    'SM': ( 0.561,  1.000,  0.780,  1.000),
    'EU': ( 0.380,  1.000,  0.780,  1.000),
    'GD': ( 0.271,  1.000,  0.780,  1.000),
    'TB': ( 0.188,  1.000,  0.780,  1.000),
    'DY': ( 0.122,  1.000,  0.780,  1.000),
    'HO': ( 0.000,  1.000,  0.612,  1.000),
    'ER': ( 0.000,  0.902,  0.459,  1.000),
    'TM': ( 0.000,  0.831,  0.322,  1.000),
    'YB': ( 0.000,  0.749,  0.220,  1.000),
    'LU': ( 0.000,  0.671,  0.141,  1.000),
    'HF': ( 0.302,  0.761,  1.000,  1.000),
    'TA': ( 0.302,  0.651,  1.000,  1.000),
    'W': ( 0.129,  0.580,  0.839,  1.000),
    'RE': ( 0.149,  0.490,  0.671,  1.000),
    'OS': ( 0.149,  0.400,  0.588,  1.000),
    'IR': ( 0.090,  0.329,  0.529,  1.000),
    'PT': ( 0.816,  0.816,  0.878,  1.000),
    'AU': ( 1.000,  0.820,  0.137,  1.000),
    'HG': ( 0.722,  0.722,  0.816,  1.000),
    'TL': ( 0.651,  0.329,  0.302,  1.000),
    'PB': ( 0.341,  0.349,  0.380,  1.000),
    'BI': ( 0.620,  0.310,  0.710,  1.000),
    'PO': ( 0.671,  0.361,  0.000,  1.000),
    'AT': ( 0.459,  0.310,  0.271,  1.000),
    'RN': ( 0.259,  0.510,  0.588,  1.000),
    'FR': ( 0.259,  0.000,  0.400,  1.000),
    'RA': ( 0.000,  0.490,  0.000,  1.000),
    'AC': ( 0.439,  0.671,  0.980,  1.000),
    'TH': ( 0.000,  0.729,  1.000,  1.000),
    'PA': ( 0.000,  0.631,  1.000,  1.000),
    'U': ( 0.000,  0.561,  1.000,  1.000),
    'NP': ( 0.000,  0.502,  1.000,  1.000),
    'PU': ( 0.000,  0.420,  1.000,  1.000),
    'AM': ( 0.329,  0.361,  0.949,  1.000),
    'CM': ( 0.471,  0.361,  0.890,  1.000),
    'BK': ( 0.541,  0.310,  0.890,  1.000),
    'CF': ( 0.631,  0.212,  0.831,  1.000),
    'ES': ( 0.702,  0.122,  0.831,  1.000),
    'FM': ( 0.702,  0.122,  0.729,  1.000),
    'MD': ( 0.702,  0.051,  0.651,  1.000),
    'NO': ( 0.741,  0.051,  0.529,  1.000),
    'LR': ( 0.780,  0.000,  0.400,  1.000),
    'RF': ( 0.800,  0.000,  0.349,  1.000),
    'DB': ( 0.820,  0.000,  0.310,  1.000),
    'SG': ( 0.851,  0.000,  0.271,  1.000),
    'BH': ( 0.878,  0.000,  0.220,  1.000),
    'HS': ( 0.902,  0.000,  0.180,  1.000),
    'MT': ( 0.922,  0.000,  0.149,  1.000),
    }

ATOMIC_NUMBERS = {
    'H': 1,
    'HE': 2,
    'LI': 3,
    'BE': 4,
    'B': 5,
    'C': 6,
    'N': 7,
    'O': 8,
    'F': 9,
    'NE': 10,
    'NA': 11,
    'MG': 12,
    'AL': 13,
    'SI': 14,
    'P': 15,
    'S': 16,
    'CL': 17,
    'AR': 18,
    'K': 19,
    'CA': 20,
    'SC': 21,
    'TI': 22,
    'V': 23,
    'CR': 24,
    'MN': 25,
    'FE': 26,
    'CO': 27,
    'NI': 28,
    'CU': 29,
    'ZN': 30,
    'GA': 31,
    'GE': 32,
    'AS': 33,
    'SE': 34,
    'BR': 35,
    'KR': 36,
    'RB': 37,
    'SR': 38,
    'Y': 39,
    'ZR': 40,
    'NB': 41,
    'MO': 42,
    'TC': 43,
    'RU': 44,
    'RH': 45,
    'PD': 46,
    'AG': 47,
    'CD': 48,
    'IN': 49,
    'SN': 50,
    'SB': 51,
    'TE': 52,
    'I': 53,
    'XE': 54,
    'CS': 55,
    'BA': 56,
    'LA': 57,
    'CE': 58,
    'PR': 59,
    'ND': 60,
    'PM': 61,
    'SM': 62,
    'EU': 63,
    'GD': 64,
    'TB': 65,
    'DY': 66,
    'HO': 67,
    'ER': 68,
    'TM': 69,
    'YB': 70,
    'LU': 71,
    'HF': 72,
    'TA': 73,
    'W': 74,
    'RE': 75,
    'OS': 76,
    'IR': 77,
    'PT': 78,
    'AU': 79,
    'HG': 80,
    'TL': 81,
    'PB': 82,
    'BI': 83,
    'PO': 84,
    'AT': 85,
    'RN': 86,
    'FR': 87,
    'RA': 88,
    'AC': 89,
    'TH': 90,
    'PA': 91,
    'U': 92,
    'NP': 93,
    'PU': 94,
    'AM': 95,
    'CM': 96,
    'BK': 97,
    'CF': 98,
    'ES': 99,
    'FM': 100,
    'MD': 101,
    'NO': 102,
    'LR': 103,
    'RF': 104,
    'DB': 105,
    'SG': 106,
    'BH': 107,
    'HS': 108,
    'MT': 109,
    }

RADII = {
    1: 1.20,
    2: 1.40,
    3: 1.82,
    4: 1.53,
    5: 1.92,
    6: 1.70,
    7: 1.55,
    8: 1.52,
    9: 1.47,
    10: 1.54,
    11: 2.27,
    12: 1.73,
    13: 1.84,
    14: 2.10,
    15: 1.80,
    16: 1.80,
    17: 1.75,
    18: 1.88,
    19: 2.75,
    20: 2.31,
    21: 2.11,
    22: 1.00,
    23: 1.00,
    24: 1.00,
    25: 1.00,
    26: 1.00,
    27: 1.00,
    28: 1.63,
    29: 1.40,
    30: 1.39,
    31: 1.87,
    32: 2.11,
    33: 1.85,
    34: 1.90,
    35: 1.85,
    36: 2.02,
    37: 3.03,
    38: 2.49,
    39: 1.00,
    40: 1.00,
    41: 1.00,
    42: 1.00,
    43: 1.00,
    44: 1.00,
    45: 1.00,
    46: 1.63,
    47: 1.72,
    48: 1.58,
    49: 1.93,
    50: 2.17,
    51: 2.06,
    52: 2.06,
    53: 1.98,
    54: 2.16,
    55: 3.43,
    56: 2.68,
    57: 1.00,
    58: 1.00,
    59: 1.00,
    60: 1.00,
    61: 1.00,
    62: 1.00,
    63: 1.00,
    64: 1.00,
    65: 1.00,
    66: 1.00,
    67: 1.00,
    68: 1.00,
    69: 1.00,
    70: 1.00,
    71: 1.00,
    72: 1.00,
    73: 1.00,
    74: 1.00,
    75: 1.00,
    76: 1.00,
    77: 1.00,
    78: 1.75,
    79: 1.66,
    80: 1.55,
    81: 1.96,
    82: 2.02,
    83: 2.07,
    84: 1.97,
    85: 2.02,
    86: 2.20,
    87: 3.48,
    88: 2.83,
    89: 1.00,
    90: 1.00,
    91: 1.00,
    92: 1.86,
    93: 1.00,
    94: 1.00,
    95: 1.00,
    96: 1.00,
    97: 1.00,
    98: 1.00,
    99: 1.00,
    100: 1.00,
    101: 1.00,
    102: 1.00,
    103: 1.00,
    104: 1.00,
    105: 1.00,
    106: 1.00,
    107: 1.00,
    108: 1.00,
    109: 1.00,
    }

ELEMENT_COLORS = {
    1: 'H',
    2: 'HE',
    3: 'LI',
    4: 'BE',
    5: 'B',
    6: 'C',
    7: 'N',
    8: 'O',
    9: 'F',
    10: 'NE',
    11: 'NA',
    12: 'MG',
    13: 'AL',
    14: 'SI',
    15: 'P',
    16: 'S',
    17: 'CL',
    18: 'AR',
    19: 'K',
    20: 'CA',
    21: 'SC',
    22: 'TI',
    23: 'V',
    24: 'CR',
    25: 'MN',
    26: 'FE',
    27: 'CO',
    28: 'NI',
    29: 'CU',
    30: 'ZN',
    31: 'GA',
    32: 'GE',
    33: 'AS',
    34: 'SE',
    35: 'BR',
    36: 'KR',
    37: 'RB',
    38: 'SR',
    39: 'Y',
    40: 'ZR',
    41: 'NB',
    42: 'MO',
    43: 'TC',
    44: 'RU',
    45: 'RH',
    46: 'PD',
    47: 'AG',
    48: 'CD',
    49: 'IN',
    50: 'SN',
    51: 'SB',
    52: 'TE',
    53: 'I',
    54: 'XE',
    55: 'CS',
    56: 'BA',
    57: 'LA',
    58: 'CE',
    59: 'PR',
    60: 'ND',
    61: 'PM',
    62: 'SM',
    63: 'EU',
    64: 'GD',
    65: 'TB',
    66: 'DY',
    67: 'HO',
    68: 'ER',
    69: 'TM',
    70: 'YB',
    71: 'LU',
    72: 'HF',
    73: 'TA',
    74: 'W',
    75: 'RE',
    76: 'OS',
    77: 'IR',
    78: 'PT',
    79: 'AU',
    80: 'HG',
    81: 'TL',
    82: 'PB',
    83: 'BI',
    84: 'PO',
    85: 'AT',
    86: 'RN',
    87: 'FR',
    88: 'RA',
    89: 'AC',
    90: 'TH',
    91: 'PA',
    92: 'U',
    93: 'NP',
    94: 'PU',
    95: 'AM',
    96: 'CM',
    97: 'BK',
    98: 'CF',
    99: 'ES',
    100: 'FM',
    101: 'MD',
    102: 'NO',
    103: 'LR',
    104: 'RF',
    105: 'DB',
    106: 'SG',
    107: 'BH',
    108: 'HS',
    109: 'MT',
    }

# Conversion factors for 1 BU. Uses nm, Angstrom or a.u. (Bohr)
# 1 BU corresponds to 1 nm
UNIT_CONV = { 
    'nm': 1.,
    'A': 0.1,
    'a.u.': 0.0529177249
    }


def create_z_orient(rot_vec):
    x_dir_p = Vector(( 1.0,  0.0,  0.0))
    y_dir_p = Vector(( 0.0,  1.0,  0.0))
    z_dir_p = Vector(( 0.0,  0.0,  1.0))
    tol = 0.001
    rx, ry, rz = rot_vec
    if isclose(rx, 0.0, abs_tol=tol) and isclose(ry, 0.0, abs_tol=tol):
        if isclose(rz, 0.0, abs_tol=tol) or isclose(rz, 1.0, abs_tol=tol):
            return Matrix((x_dir_p, y_dir_p, z_dir_p))  # 3x3 identity
    new_z = rot_vec.copy()  # rot_vec already normalized
    new_y = np.cross(new_z, z_dir_p) #new_z.cross(z_dir_p)
    new_y_eq_0_0_0 = True
    for v in new_y:
        if not isclose(v, 0.0, abs_tol=tol):
            new_y_eq_0_0_0 = False
            break
    if new_y_eq_0_0_0:
        new_y = y_dir_p
    new_x = np.cross(new_y, new_z) #new_y.cross(new_z)
    new_x /= np.linalg.norm(new_x)
    new_y /= np.linalg.norm(new_y)
    return Matrix(((new_x[0], new_y[0], new_z[0]),
                   (new_x[1], new_y[1], new_z[1]),
                   (new_x[2], new_y[2], new_z[2])))

def jet_colormap(x, alpha=1.):
    """Return blender 4-tuple for RGBA color of jet color map from x in range 0 to 1"""

    if x < 0.1:
        r = 0.
        g = 0.
        b = 0.6 + 0.4 * x / 0.1
    elif x < 0.35:
        r = 0.
        g = (x - 0.1) / 0.25
        b = 1.
    elif x < 0.65:
        r = (x - 0.35) / 0.3
        g = 1.
        b = 1. - (x - 0.35) / 0.3
    elif x < 0.9:
        r = 1.
        g = 1. - (x - 0.65) / 0.25
        b = 0.
    else:
        r = 1. - 0.4 * (x - 0.9) / 0.1
        g = 0.
        b = 0.

    return ( r, g, b, alpha )

def _create_new_surfacevertexpaint_material(name, color, alpha=1.):
    """Create a new material.

    Args:
        name (str): Name for the new material (e.g., 'red')
        color (tuple): RGB color for the new material (diffuse_color) 
            (e.g., (1, 0, 0))
    Returns:
        The new material.
    """
    
    nodescale = 300

    mat = bpy.data.materials.new(name)
    mat.use_nodes = True
    mat.blend_method = 'HASHED' # enable alpha
    nodes = mat.node_tree.nodes
    nodes.clear()

    vert = nodes.new(type="ShaderNodeVertexColor")
    vert.location = -1*nodescale, 0

    bsdf = nodes.new(type="ShaderNodeBsdfPrincipled")
    bsdf.location = 0*nodescale, 0
    bsdf.inputs['Base Color'].default_value = color
    #bsdf.inputs['Metallic'].default_value = 0.1
    bsdf.inputs['Specular'].default_value = 0.5
    bsdf.inputs['Roughness'].default_value = 0.01
    bsdf.inputs['Anisotropic'].default_value = 0.5
    bsdf.inputs['Clearcoat'].default_value = 0.7
    bsdf.inputs['Clearcoat Roughness'].default_value = 0.03
    #bsdf.inputs['Transmission'].default_value = 1.
    #bsdf.inputs['Transmission Roughness'].default_value = 0.4
    bsdf.inputs['Alpha'].default_value = alpha


    node_output = nodes.new(type="ShaderNodeOutputMaterial")
    node_output.location = 1*nodescale, 0

    links = mat.node_tree.links
    link = links.new(vert.outputs[0], bsdf.inputs[0])
    link = links.new(bsdf.outputs[0], node_output.inputs[0])
    return mat

def _create_new_material(name, color, alpha=1.):
    """Create a new material.

    Args:
        name (str): Name for the new material (e.g., 'red')
        color (tuple): RGB color for the new material (diffuse_color) 
            (e.g., (1, 0, 0))
    Returns:
        The new material.
    """
    
    print(f'Creating new material {name} with color {color}')
    nodescale = 300

    mat = bpy.data.materials.new(name)
    mat.use_nodes = True
    nodes = mat.node_tree.nodes
    nodes.clear()

    bsdf = nodes.new(type="ShaderNodeBsdfPrincipled")
    bsdf.location = 2*nodescale, 0
    bsdf.inputs[0].default_value = color
    bsdf.inputs[18].default_value = alpha

    #ao = nodes.new(type="ShaderNodeAmbientOcclusion")
    #ao.location = 0*nodescale, 0

    #val_to_rgb = nodes.new(type="ShaderNodeValToRGB")
    #val_to_rgb.location = 1*nodescale, 0
    #elements = val_to_rgb.color_ramp.elements
    ## 2 elements are there by default. adding 2 more
    #elements.new(0.)
    #elements.new(0.)
    #elements[0].color = color
    #elements[0].position = 0.
    #elements[1].color = COLORS['black']
    #elements[1].position = 0.03
    #elements[2].color = COLORS['black']
    #elements[2].position = 0.5
    #elements[3].color = color
    #elements[3].position = 1.


    

    node_output = nodes.new(type="ShaderNodeOutputMaterial")
    node_output.location = 3*nodescale, 0

    links = mat.node_tree.links
    #link = links.new(ao.outputs[0], val_to_rgb.inputs[0])
    #link = links.new(val_to_rgb.outputs[0], bsdf.inputs[0])
    link = links.new(bsdf.outputs[0], node_output.inputs[0])
    return mat

    ### old version
    mat = bpy.data.materials.new(name)
    mat.diffuse_color = color
    #mat.diffuse_shader = 'OREN_NAYAR'
    #mat.diffuse_intensity = 0.8
    mat.roughness = 0.5
    mat.specular_color = (1, 1, 1)
    #mat.specular_shader = 'BLINN'
    mat.specular_intensity = 0.2
    #mat.specular_hardness = 25
    #mat.ambient = 1
    #mat.use_transparent_shadows = True
    # mat.subsurface_scattering.use = True

    return mat


def _create_new_surface_material(name, color, alpha=1.):
    """Create a new material.

    Args:
        name (str): Name for the new material (e.g., 'red')
        color (tuple): RGB color for the new material (diffuse_color) 
            (e.g., (1, 0, 0))
    Returns:
        The new material.
    """
    
    nodescale = 300

    mat = bpy.data.materials.new(name)
    mat.use_nodes = True
    mat.blend_method = 'HASHED' # enable alpha
    nodes = mat.node_tree.nodes
    nodes.clear()

    bsdf = nodes.new(type="ShaderNodeBsdfPrincipled")
    bsdf.location = 0*nodescale, 0
    bsdf.inputs['Base Color'].default_value = color
    #bsdf.inputs['Metallic'].default_value = 0.1
    bsdf.inputs['Specular'].default_value = 0.5
    bsdf.inputs['Roughness'].default_value = 0.01
    bsdf.inputs['Anisotropic'].default_value = 0.5
    bsdf.inputs['Clearcoat'].default_value = 0.7
    bsdf.inputs['Clearcoat Roughness'].default_value = 0.03
    #bsdf.inputs['Transmission'].default_value = 1.
    #bsdf.inputs['Transmission Roughness'].default_value = 0.4
    bsdf.inputs['Alpha'].default_value = alpha


    node_output = nodes.new(type="ShaderNodeOutputMaterial")
    node_output.location = 1*nodescale, 0

    links = mat.node_tree.links
    link = links.new(bsdf.outputs[0], node_output.inputs[0])
    return mat

# spherical harmonics in python
# https://docs.scipy.org/doc/scipy/reference/generated/scipy.special.sph_harm.html
# https://stackoverflow.com/questions/4116658/faster-numpy-cartesian-to-spherical-coordinate-conversion

# https://www.ks.uiuc.edu/Research/vmd/plugins/doxygen/moldenplugin_8c-source.html
#00914   /* If we have MOs in the file we must provide the 
#00915    * angular momentum exponents.
#00916    * The order of P, D, F en G functions is as follows:
#00917 
#00918    *  5D: D 0, D+1, D-1, D+2, D-2
#00919    *  6D: xx, yy, zz, xy, xz, yz
#00920 
#00921    *  7F: F 0, F+1, F-1, F+2, F-2, F+3, F-3
#00922    * 10F: xxx, yyy, zzz, xyy, xxy, xxz, xzz, yzz, yyz, xyz
#00923 
#00924    *  9G: G 0, G+1, G-1, G+2, G-2, G+3, G-3, G+4, G-4
#00925    * 15G: xxxx yyyy zzzz xxxy xxxz yyyx yyyz zzzx zzzy,
#00926    *      xxyy xxzz yyzz xxyz yyxz zzxy
#00927    */

# also available in molden source gaussian.f

class PGF:
    """Primitive Gaussian Function"""

    def __init__(self, location, shell, angmom, alpha, coeff):
        self.location = location
        self.shell = shell # integer 1, 2, 3, ...
        self.angmom = angmom # string of type s, px, py, py, dxy, ...
        self.alpha = alpha
        self.coeff = coeff # contraction coefficient

    def __repr__(self):
        return f"{self.shell}{self.angmom} PGF at {self.location}; alpha={self.alpha}; coeff={self.coeff}"

    def calc(self, pos, unit='nm', diffuse=True):
        """Return primitive Gaussian value at position"""
        
        r = (self.location - pos) #/ UNIT_CONV['a.u.']
        # scale distance to atomic units for calculation
        r *= UNIT_CONV[unit] / UNIT_CONV['a.u.']
        r_abs = np.linalg.norm(r)
        
        if not diffuse and r_abs > 9.4:
            return 0.
        
        ### define angmom as string (x2-y2, dz2, ...) because this formula doesnt give all orbitals
        if self.angmom == 's':
            angular = -1.
            l = 0
            m = 0
        elif self.angmom == 'px':
            angular = r[0]
            l = 1
            m = -1
        elif self.angmom  == 'py':
            angular = r[1]
            l = 1
            m = 1
        elif self.angmom == 'pz':
            angular = r[2]
            l = 1
            m = 0
        else:
            # get spherical angles
            # phi is polar angle (this is SciPy convention)
            # theta is azimuthal angle
            # in literature it is quite common to use names the other way around
            l = 'dfg'.find(self.angmom[0].lower()) + 2
            m = int(self.angmom[1:])
            #print(m,l)

            theta = math.atan2(r[1], r[0])
            phi = math.atan2(r[2], (r[0]**2 + r[1]**2)**0.5) + np.pi/2
            #print(theta, phi)
            angular = sph_harm(m, l, theta, phi).real
        
        # http://www.theochem.ru.nl/~pwormer/Knowino/knowino.org/wiki/Solid_harmonics.html
        # what about factor r_abs**l ?
        return self.coeff * angular * np.exp(-self.alpha*r_abs**2) * r_abs**(self.shell)
        return self.coeff * ( ( 4*np.pi)/(2*l + 1) )**0.5 * r_abs**l * angular * np.exp(-self.alpha*np.dot(r,r))
            
        return self.coeff * angular * r_abs**self.shell * np.exp(-self.alpha*np.dot(r,r))

class CGF:
    """Contracted Gaussian Function constructed from linear combination of several PGFs"""

    def __init__(self, location, shell_counter, shell, alphas, coeffs):
        self.location = location # atom position
        self.shell_counter = shell_counter # 1, 2, 3, ...
        self.shell = shell # type of orbital: s, px, py, pz, ...
        self.alphas = alphas # alpha coefficients in exponent of primitives
        self.coeffs = coeffs # contraction coefficients

        self.pgfs = [] # list of primitives
        for i in range(len(self.coeffs)):
            self.pgfs.append( PGF(self.location, self.shell_counter, self.shell, self.alphas[i], self.coeffs[i]) )
    
    def __repr__(self):
        outstr = f"CGF type {self.shell_counter}{self.shell} at {self.location} constructed from {len(self.pgfs)} primitives"
        for pgf in self.pgfs:
            outstr += f"\n    {pgf}"
        return outstr

    def calc(self, position, unit='nm', diffuse=True):

        tmp = 0.
        for pgf in self.pgfs: 
            tmp += pgf.calc(position, unit, diffuse)
        #tmp += self.pgfs[0].calc(position, unit, diffuse)
        
        return tmp

class STO:
    """Slater Type Orbital"""

    def __init__(self, location, kx, ky, kz, kr, alpha, bnorm):
        self.location = location
        self.kx = kx
        self.ky = ky
        self.kz = kz
        self.kr = kr
        self.alpha = alpha
        if (kx + ky + kz + 1) % 2 == 0:
            sign = 1.
        else:
            sign = -1.
        self.bnorm = bnorm * sign
    
    def __repr__(self):
        return f"STO ({self.kx} {self.ky} {self.kz} {self.kr}; alpha={self.alpha}, bnorm={self.bnorm}) at {self.location}"
    
    def calc(self, pos, unit='nm', diffuse=True):
        """Return STO value at given position. (see http://www.theochem.ru.nl/~pwormer/Knowino/knowino.org/wiki/Slater_orbital.html )"""
        
        r = self.location - pos #/ UNIT_CONV['a.u.']
        # scale distance to atomic units for calculation
        r *= UNIT_CONV[unit] / UNIT_CONV['a.u.']
        
        x = r[0]
        y = r[1]
        z = r[2]
        r = np.linalg.norm(r)
        
        if not diffuse and r > 9.4:
            return 0.
        
        return x**self.kx * y**self.ky * z**self.kz * r**self.kr * self.bnorm * np.exp(-self.alpha * r)
        



class GTO:
    """Gaussian Type Orbital"""

    def __init__(self, location, i, j, k, alpha):
        self.location = location
        self.i = i
        self.j = j
        self.k = k
        self.alpha = alpha
    
    def calc(self, pos):
        """Return GTO value at given position. (see http://www.theochem.ru.nl/~pwormer/Knowino/knowino.org/wiki/Gaussian_type_orbitals.html )"""
        r = self.location - pos

        x = r[0]
        y = r[1]
        z = r[2]

        r2 = x*x + y*y + z*z

        return x**self.i * y**self.j * z**self.k * np.exp(-self.alpha*r2)

def make_cube(f, ref_cube=None):
    """Create new Volume object and populate it with values taken from f.calc at grid points"""
    
    if ref_cube is None:
        offset = -5.
        size = 10.
        points = 50
        units = 'A'
        # setting up cube
        print(f"Offset: {offset} {units} in all dimensions")
        print(f"Box size: {size} {units}")
        print(f"Resolution: {points} points in all dimensions")
        offset = np.array( [offset, offset, offset] )
        ng = [ points, points, points ]
        dg = size / points
        basis = [ np.array( [dg, 0., 0.] ),
                  np.array( [0., dg, 0.] ),
                  np.array( [0., 0., dg] ) ]
        ref_cube = Volume("ref", offset, ng, basis)
    else:
        print(f"Using resolution and size of {ref_cube.name}")

    print("Returning Volume object of function")
    cube = Volume('function', ref_cube.offset, ref_cube.ng, ref_cube.basis)

    offset = cube.offset
    ng = cube.ng
    basis = cube.basis
    data = cube.data
    
    # for every data point
    for ix in range(ng[0]):
        for iy in range(ng[1]):
            for iz in range(ng[2]):
                # get position of point
                pos = offset + ix * basis[0] + iy * basis[1] + iz * basis[2]
                data[ix][iy][iz] = f.calc(pos)
    
    print(f'Values range from {np.amin(data)} to {np.amax(data)}')

    return cube

class MoldenOrbitals:
    """Saves information of AOs and MOs"""

    def __init__(self, name, aos, mos):

        self.name = name
        self.aos = aos
        self.mos = mos

    def __repr__(self):
        outstr = f"{self.name}\nContaining {len(self.aos)} AOs and {len(self.mos)} MOs\n"
        for i in range(len(self.mos)):
            outstr += 'MO % 3i: E= % 3.2f occup % 1.2f\n' % (i, self.mos[i]['ene'], self.mos[i]['occup'])
        return outstr

    def calc(self,pos, mo_index, mo=True, diffuse=True, coeff_thresh_scale=0.1, units='nm'):
        if mo: # calculate MO
            max_coeff = np.amax(np.abs(self.mos[mo_index]['coeffs']))
            
            coeff_thresh = max_coeff * coeff_thresh_scale
            tmp = 0.
            for i in range(len(self.aos)):
                if abs(self.mos[mo_index]['coeffs'][i]) > coeff_thresh:
                    print(i, max_coeff, self.mos[mo_index]['coeffs'][i])
                    tmp += self.mos[mo_index]['coeffs'][i] * self.aos[i].calc(pos,units,diffuse)
            return tmp
        else: # calculate AO
            return self.aos[mo_index].calc(pos, units, diffuse)
            
    
    def draw_orbital(self, name="orbital", index=0, resolution=25,
                    thresh=0.02, mo=True, alpha=0.7, units='nm', diffuse=True, coeff_thresh=0.1):
                        
        print(f"Drawing orbital {index} (mo={mo})")
        if mo:
            print(self.mos[index])
        else:
            print(self.aos[index])
        # create Volume object of orbital data
        cube = self.make_cube(mo_index=index, resolution=resolution, mo=mo,
                                diffuse=diffuse, coeff_thresh=coeff_thresh)
        
        # draw orbital surface
        cube.draw_orbital(name, thresh, alpha, units)
        return cube
    
    def make_cube(self, mo_index=0, resolution=25, mo=True, ref_cube=None, diffuse=True, coeff_thresh=0.1):
        """Create new Volume object and populate it with values taken from f.calc at grid points"""
    
        if ref_cube is None:
            # calculate average position and good size to fit orbitals in cube
            ao_x = [ self.aos[i].location[0] for i in range(len(self.aos)) ]
            ao_y = [ self.aos[i].location[1] for i in range(len(self.aos)) ]
            ao_z = [ self.aos[i].location[2] for i in range(len(self.aos)) ]

            # box contains molecule + some Ang in every direction
            vac = 0.2 # BU (nm) vacuum around molecule
            offset = np.array( [np.amin(ao_x) - vac, np.amin(ao_y) - vac, np.amin(ao_z) - vac] )
            size = np.array( [ np.max(ao_x) - np.min(ao_x) + 2*vac,
                               np.max(ao_y) - np.min(ao_y) + 2*vac,
                               np.max(ao_z) - np.min(ao_z) + 2*vac ] )
            
            points = resolution
            units = 'nm'

            # setting up cube
            #offset = np.array( [offset, offset, offset] )
            ng = [ points, points, points ]
            #dg = size / points
            basis = [ np.array( [size[0] / points, 0., 0.] ),
                      np.array( [0., size[1] / points, 0.] ),
                      np.array( [0., 0., size[2]/points ] ) ]
            ref_cube = Volume("ref", offset, ng, basis)
            
            print(f"Offset: {offset} {units}")
            print(f"Box size: {size} {units}")
            print(f"Resolution: {points} points in all dimensions")
            print(f"Basis is: {basis}")
        else:
            print(f"Using resolution and size of {ref_cube.name}")

        print("Returning Volume object of function")
        cube = Volume('function', ref_cube.offset, ref_cube.ng, ref_cube.basis)

        offset = cube.offset
        ng = cube.ng
        basis = cube.basis
        data = cube.data
        
        # progress counter
        max_progress = ng[0]
        bpy.context.window_manager.progress_begin(0, 100)
    
        # for every data point
        for ix in range(ng[0]):
            
            print(f"Progress {ix+1} / {ng[0]}")
            bpy.context.window_manager.progress_update(int(100. * float(ix+1)/max_progress))
            
            for iy in range(ng[1]):
                for iz in range(ng[2]):
                    # get position of point
                    pos = offset + ix * basis[0] + iy * basis[1] + iz * basis[2]
                    #print(pos)
                    data[ix][iy][iz] = self.calc(pos, mo_index, mo, diffuse, coeff_thresh)
    
        bpy.context.window_manager.progress_end()
        print(f'Values range from {np.amin(data)} to {np.amax(data)}')

        return cube


class Atom:
    """A single atom.

    Attributes:
        at_num (int): The atomic number of the atom.
        location (numpy array): The xyz location of the atom, in 
            Angstroms.
        id_num (int): A unique identifier number.
    """

    def __init__(self, atomic_number, location, id_num):
        self.at_num = atomic_number
        self.location = location # np.array
        self.id_num = id_num

    def draw(self, color='by_element', radius=None, units='nm', 
             scale=1.0, subsurf_level=0, segments=16):
        """Draw the atom in Blender.

        Args:
            color (string, ='by_element'): If None, coloring is done by
                element. Otherwise specifies the color.
            radius (string, =None): If None, draws at the van der Waals
                radius. Otherwise specifies the radius in angstroms.
            units (sting, ='nm'): 1 BU = 1 nm by default. Can also be
                set to angstroms.
            scale (float, =1.0): Scaling factor for the atom. Useful
                when generating ball-and-stick models.
            subsurf_level (int, =2): Subsurface subdivisions that will
                be applied.
            segments (int, =16): Number of segments in each UV sphere
                primitive

        Returns:
            The blender object.
        """

        # The corrected location (i.e., scaled to units.)
        loc_corr = tuple(c*UNIT_CONV[units] for c in self.location)

        # Work out the sphere radius in BU.
        if not radius:
            rad_adj = UNIT_CONV['A']*RADII[self.at_num]*UNIT_CONV[units]*scale
        else:
            rad_adj = radius*UNIT_CONV[units]*scale

        # Create sphere as bmesh.
        bm = bmesh.new()
        bmesh.ops.create_uvsphere(bm, 
                                  u_segments=segments, 
                                  v_segments=segments,
                                  radius=rad_adj)
        
        for f in bm.faces:
            f.smooth = True
        
        # Convert to mesh.
        me = bpy.data.meshes.new("Mesh")
        bm.to_mesh(me)
        bm.free()

        # Assign mesh to object and place in space.
        atom_sphere = bpy.data.objects.new("atom({})_{}".format(
                            self.at_num, self.id_num), me)
        bpy.context.collection.objects.link(atom_sphere)
        
        atom_sphere.location = loc_corr

        # Assign subsurface modifier, if requested
        if subsurf_level != 0:
            atom_sphere.modifiers.new('Subsurf', 'SUBSURF')
            atom_sphere.modifiers['Subsurf'].levels = subsurf_level

        # Color atom and assign material
        if color == 'by_element':
            atom_color = ELEMENT_COLORS[self.at_num]
        else:
            atom_color = color

        if not atom_color in bpy.data.materials:
            _create_new_material(atom_color, COLORS[atom_color])

        atom_sphere.data.materials.append(bpy.data.materials[atom_color])

        return atom_sphere


class Bond:
    """A bond between two atoms.

    Attributes:
        atom1 (atom): The first atom in the bond.
        atom2 (atom): The second atom in the bond.
    """

    def __init__(self, atom1, atom2):
        self.atom1 = atom1
        self.atom2 = atom2

    @staticmethod
    def _draw_half(location, length, rot_angle, rot_axis, element, 
                   radius=0.02, color='by_element', units='nm',
                   vertices=64, edge_split=False, start=0, end=0):
        """Draw half of a bond (static method).

        Draws half of a bond, given the location and length. Bonds are
        drawn in halves to facilitate coloring by element.

        Args:
            location (np.array): The center point of the half bond.
            length (float): The length of the half bond.
            rot_angle (float): Angle by which bond will be rotated.
            rot_axis (np.array): Axis of rotation.
            element (int): atomic number of element of the bond (for 
                coloring).
            radius (float, =0.2): radius of the bond.
            color (string, ='by_element'): color of the bond. If
                'by_element', uses element coloring.
            units (string, ='nm'): 1 BU = 1 nm, by default. Can change
                to angstroms ('A').
            vertices (int, =64): Number of vertices in each bond
                cylinder.
            edge_split (bool, =False): Whether to apply the edge split
                modifier to each bond.

        Returns:
            The new bond (Blender object).
        """

        loc_corr = tuple(c*UNIT_CONV[units] for c in location)
        len_corr = length * UNIT_CONV[units]
        radius_corr = radius * UNIT_CONV[units]
        start = start * UNIT_CONV[units]
        end = end * UNIT_CONV[units]
        if False:
            bpy.ops.mesh.primitive_cylinder_add(vertices=vertices,
                                            radius=radius_corr, 
                                            depth=len_corr, location=loc_corr, 
                                            end_fill_type='NOTHING')
            print(rot_axis)
            rot_matrix = create_z_orient(rot_axis)
            bpy.ops.transform.rotate(value=rot_angle, orient_axis='Z', orient_matrix=rot_matrix)
        else:
            dx = end[0] - start[0]
            dy = end[1] - start[1]
            dz = end[2] - start[2]
            dist = math.sqrt(dx**2 + dy**2 + dz**2)

            bpy.ops.mesh.primitive_cylinder_add(vertices=vertices,
                                            radius=radius_corr, 
                                            depth=len_corr, location=(dx/2+start[0], dy/2+start[1], dz/2+start[2]), 
                                            end_fill_type='NOTHING')            

            phi = math.atan2(dy, dx) 
            theta = math.acos(dz/dist) 

            bpy.context.object.rotation_euler[1] = theta 
            bpy.context.object.rotation_euler[2] = phi 
        
        bpy.ops.object.shade_smooth()

        if edge_split:
            bpy.ops.object.modifier_add(type='EDGE_SPLIT')
            bpy.ops.object.modifier_apply(modifier='EdgeSplit')

        if color == 'by_element':
            bond_color = ELEMENT_COLORS[element]
        else:
            bond_color = color

        if not bond_color in bpy.data.materials:
            _create_new_material(bond_color, COLORS[bond_color])

        bpy.context.object.data.materials.append(
            bpy.data.materials[bond_color])

        return bpy.context.object


    def draw(self, radius=0.02, color='by_element', units='nm',
             vertices=64, edge_split=False):
        """Draw the bond as two half bonds (to allow coloring).

        Args:
            radius (float, =0.2): Radius of cylinder in angstroms.
            color (string, ='by_element'): Color of the bond. If
                'by_element', each half gets element coloring.
            units (string, ='nm'): 1 BU = 1 nm, by default. Can change
                to angstroms ('A').
            vertices (int, =64): Number of vertices in each bond
                cylinder.
            edge_split (bool, =False): Whether to apply the edge split
                modifier to each bond.

        Returns:
            The bond (Blender object), with both halves joined.
        """

        created_objects = []

        center_loc = (self.atom1.location + self.atom2.location)/2
        bond_vector = self.atom1.location - self.atom2.location
        length = np.linalg.norm(bond_vector)

        bond_axis = bond_vector/length
        cyl_axis = np.array((0,0,1))
        rot_axis = np.cross(bond_axis, cyl_axis)
        angle = -np.arccos(np.dot(cyl_axis, bond_axis))

        start_center = (self.atom1.location + center_loc)/2
        created_objects.append(Bond._draw_half(start_center, length/2, angle, 
                               rot_axis, self.atom1.at_num, radius, color, 
                               units, vertices, edge_split, self.atom1.location, center_loc))

        end_center = (self.atom2.location + center_loc)/2
        created_objects.append(Bond._draw_half(end_center, length/2, angle, 
                               rot_axis, self.atom2.at_num, radius, color, 
                               units, vertices, edge_split, center_loc, self.atom2.location))

        for obj in bpy.context.selected_objects:
            obj.select_set(state=False)

        for obj in created_objects:
            obj.select_set(state=True)

        bpy.ops.object.join()
        bpy.context.object.name = "bond_{}({})_{}({})".format(
            self.atom1.id_num, self.atom1.at_num, self.atom2.id_num, 
            self.atom2.at_num)
        
        return bpy.context.object

class Molecule:
    """The molecule object.

    Attributes:
        atoms (list, = []): List of atoms (atom objects) in molecule.
        bonds (list, = []): List of bonds (bond objects) in molecule.

    Atom coordinates are saved in Angstrom internally.
    """

    def __init__(self, name='molecule', atoms=None, bonds=None, normal_modes=None):
        self.name = name
        if atoms == None:
            self.atoms = []
        else:
            self.atoms = atoms
        if bonds == None:
            self.bonds = []
        else:
            self.bonds = bonds
        self.normal_modes = normal_modes
    
    def __repr__(self):
        outstr = f"Molecule {self.name}\n{len(self.atoms)} atoms\n{len(self.bonds)} bonds"
        return outstr

    def add_atom(self, atom):
        """Adds an atom to the molecule."""
        self.atoms.append(atom)

    def add_bond(self, a1id, a2id):
        """Adds a bond to the molecule, using atom ids."""
        if not self.search_bondids(a1id, a2id):
            self.bonds.append(Bond(self.search_atomid(a1id), 
                                   self.search_atomid(a2id)))

    def search_atomid(self, id_to_search):
        """Searches through atom list and returns atom object
        corresponding to (unique) id."""
        for atom in self.atoms:
            if atom.id_num == id_to_search:
                return atom
        return None
    
    def guess_bonds(self, threshold=0.15):
        """Calculate distance matrix between all atom coordinates
        and guess bonds between atoms."""
        coords = np.array( [ self.atoms[i].location for i in range(len(self.atoms)) ] )
        ids = [ self.atoms[i].id_num for i in range(len(self.atoms)) ]
        print(ids)
        #print(coords)
        dists = distance_matrix(coords, coords)
        print(dists)
        for x in range(1,len(dists)):
            for y in range(0, x):
                if dists[x][y] < threshold and x != y:
                    try:
                        print(f"{ids[x]} {ids[y]}: {dists[x][y]} < {threshold}, atnum: {self.search_atomid(ids[x]).at_num} {self.search_atomid(ids[y]).at_num}")
                    except AttributeError:
                        print(x, y, ids[x], idx[y])
                        raise
                    if (self.search_atomid(ids[x]).at_num == 1) or (self.search_atomid(ids[y]).at_num == 1):
                        # if one or both atoms are hydrogens add bond only if distance is smaller than reduced threshold
                        if dists[x][y] < threshold*0.75:
                            self.add_bond(ids[x], ids[y])
                    else:
                        # add bond
                        self.add_bond(ids[x], ids[y])
        #print(self.bonds)
        #print(f'Molecule contains {len(self.bonds)} bonds')

    def search_bondids(self, id1, id2):
        """Searches through bond list and returns bond object
        corresponding to (unique) ids."""
        for b in self.bonds:
            if ((id1, id2) == (b.atom1.id_num, b.atom2.id_num) or
                    (id2, id1) == (b.atom1.id_num, b.atom2.id_num)):
                return b
        return None
    
    def draw_vdw(self, size=2., points=25, offset=-1., units='nm', ref_cube=None):

        print(f"Calculating and drawing van-der-Waals surface for molecule {self.name}")
        


        if ref_cube is None:
            
            # calculate average position and good size to fit orbitals in cube
            ao_x = [ self.atoms[i].location[0] for i in range(len(self.atoms)) ]
            ao_y = [ self.atoms[i].location[1] for i in range(len(self.atoms)) ]
            ao_z = [ self.atoms[i].location[2] for i in range(len(self.atoms)) ]

            # box contains molecule + some Ang in every direction
            vac = 0.5 # BU (nm) vacuum around molecule
            offset = np.array( [np.amin(ao_x) - vac, np.amin(ao_y) - vac, np.amin(ao_z) - vac] )
            size = np.array( [ np.max(ao_x) - np.min(ao_x) + 2*vac,
                               np.max(ao_y) - np.min(ao_y) + 2*vac,
                               np.max(ao_z) - np.min(ao_z) + 2*vac ] )
            
            units = 'nm'            
            
            
            
            # setting up cube
            #offset = np.array( [offset, offset, offset] )
            ng = [ points, points, points ]
            #dg = size / points
            basis = [ np.array( [size[0] / points, 0., 0.] ),
                      np.array( [0., size[1] / points, 0.] ),
                      np.array( [0., 0., size[2]/points ] ) ]
            ref_cube = Volume("ref", offset, ng, basis)
            
            print(f"Offset: {offset} {units}")
            print(f"Box size: {size} {units}")
            print(f"Resolution: {points} points in all dimensions")
            print(f"Basis is: {basis}")

        else:
            print(f"Using resolution and size of {ref_cube.name}")
        print("Returning Volume object of van-der-Waals surface")

        vdw = self.calculate_vdw(ref_cube)

        vdw.draw_surface(name=f"vdW of {self.name}", thresh=0.5, units=units)

        return vdw


    def calculate_vdw(self, ref_cube, scale=1.):
        """Calculate and draw van der Waals surface of molecule.
        
        A new cube is generated and attached to the molecule's cubes-list.
        It contains information of each data point is in/out the vdW surface.
        """
        
        print('Calculating van-der-Waals surface cube data.')
        
        cube = Volume(f'van-der-Waals surface of molecule {self.name}',
                      ref_cube.offset, ref_cube.ng, ref_cube.basis)

        offset = cube.offset
        ng = cube.ng
        basis = cube.basis
        data = cube.data
        
        print("Grid offset: ", offset)
        print("Grid size: ", ng[0], ng[1], ng[2])
        print("Basis vectors for grid are:")
        for abc in range(3):
            print(basis[abc])

        # threshold for smooth plotting
        dist_min = 0.5 # relative distance with respect to vdW radius at which cube data becomes smaller
        dist_max = 1.5 # relative distance at which cube data is 0

        # for every data point
        for ix in range(ng[0]):
            for iy in range(ng[1]):
                for iz in range(ng[2]):
                    # get position of point
                    pos = offset + ix * basis[0] + iy * basis[1] + iz * basis[2]

                    # for every atom in molecule
                    for atom in self.atoms:
                        rel_dist = np.linalg.norm(atom.location - pos) / (UNIT_CONV['A']*RADII[atom.at_num] * scale)

                        if rel_dist < dist_min:
                            data[ix][iy][iz] = 1.
                        elif rel_dist < dist_max:
                            # calculate falloff for better plotting results
                            #vdw = np.cos((rel_dist - dist_min) / (dist_max - dist_min) * np.pi / 2)**2 # cos^2 falloff
                            vdw = 1. - (rel_dist - dist_min) / (dist_max - dist_min) # linear falloff
                            if data[ix][iy][iz] < vdw:
                                data[ix][iy][iz] = vdw
        
        #self.cubes.append(cube)
        #print("Cube data inserted at index", len(self.cubes)-1)

        return cube


    def draw_bonds(self, caps=True, radius=0.02, color='by_element', 
                   units='nm', join=True, with_H=True, subsurf_level=0,
                   vertices=64, edge_split=False):
        """Draws the molecule's bonds.

        Args:
            caps (bool, =True): If true, each bond capped with sphere of
                radius at atom position. Make false if drawing
                ball-and-stick model using separate atom drawings.
            radius (float, =0.2): Radius of bonds in angstroms.
            color (string, ='by_element'): Color of the bonds. If
                'by_element', each gets element coloring.
            units (string, ='nm'): 1 BU = 1 nm, by default. Can change
                to angstroms ('A').
            join (bool, =True): If true, all bonds are joined together
                into a single Bl object.
            with_H (bool, =True): Include H's.
            subsurf_level (int, =1): Subsurface subdivisions that will
                be applied to the atoms (end caps).
            vertices (int, =64): Number of vertices in each bond
                cylinder.
            edge_split (bool, =False): Whether to apply the edge split
                modifier to each bond.

        Returns:
            The bonds as a single Blender object, if join=True.
            Otherwise, None.
        """
        
        if len(self.bonds) == 0:
            print("Warning: There are no bonds in this molecule. Maybe use guess_bonds().")
            return

        created_objects = []

        for b in self.bonds:
            if with_H or ( b.atom1.at_num != 1 and b.atom2.at_num != 1 ):
                created_objects.append(b.draw(radius=radius,
                                              color=color, 
                                              units=units, 
                                              vertices=vertices,
                                              edge_split=edge_split))

        if caps:
            for a in self.atoms:
                if with_H or a.at_num != 1:
                    created_objects.append(a.draw(color=color, 
                                                  radius=radius, 
                                                  units=units,
                                                  subsurf_level=subsurf_level))
        
        if join:
            # Deselect anything currently selected.
            for obj in bpy.context.selected_objects:
                obj.select_set(state=False)

            # Select drawn bonds.
            for obj in created_objects:
                obj.select_set(state=True)

            if len(created_objects) > 0:
                bpy.ops.object.join()


            bpy.context.object.name = self.name + '_bonds'
            
            return bpy.context.object

        else:
            return None


    def draw_atoms(self, color='by_element', radius=None, units='nm', 
                   scale=1.0, join=True, with_H=True, subsurf_level=0,
                   segments=16):
        """Draw spheres for all atoms.

        Args: 
            color (str, ='by_element'): If 'by_element', uses colors in
                ELEMENT_COLORS. Otherwise, can specify color for whole
                model. Must be defined in COLORS.
            radius (float, =None): If specified, gives radius of all 
                atoms.
            units (str, ='nm'): Units for 1 BU. Can also be A.
            join (bool, =True): If true, all atoms are joined together
                into a single Bl object.
            with_H (bool, =True): Include the hydrogens.
            subsurf_level (int, =2): Subsurface subdivisions that will
                be applied to the atoms.
            segments (int, =16): Number of segments in each UV sphere
                primitive

        Returns:
            The atoms as a single Blender object, if join=True.
            Otherwise, None.
        """

        # Store start time to time script.
        start_time = time.time()

        # Holds links to all created objects, so that they can be
        # joined.
        created_objects = []

        # Initiate progress monitor over mouse cursor.
        bpy.context.window_manager.progress_begin(0, len(self.atoms))
        
        n = 0
        for a in self.atoms:
            if with_H or a.at_num != 1:
                created_objects.append(a.draw(color=color, radius=radius, 
                                              units=units, scale=scale,
                                              subsurf_level=subsurf_level,
                                              segments=segments))
            n += 1
            bpy.context.window_manager.progress_update(n)

        # End progress monitor.
        bpy.context.window_manager.progress_end()

        if join:
            # Deselect all objects in scene.
            for obj in bpy.context.selected_objects:
                obj.select_set(state=False)
            # Select all newly created objects.
            for obj in created_objects:
                obj.select_set(state=True)
            bpy.context.view_layer.objects.active = created_objects[0]
            bpy.ops.object.join()
            bpy.context.object.name = self.name + '_atoms'
            
        print("{} seconds".format(time.time()-start_time))
        return bpy.context.object

    def draw_bs(self, color='by_element', radius=None, units='nm', scale=.3, join=True, with_H=True, subsurf_level=0, segments=16):
        
        ob = self.draw_atoms(color=color, radius=radius, units=units, scale=scale, join=join, with_H=with_H, subsurf_level=subsurf_level, segments=segments)
        if len(self.bonds) > 0:
            ob2 = self.draw_bonds(color=color, with_H=with_H, vertices=segments)
            created_objects = [ob, ob2]
        else:
            created_objects = [ob]
            
        # Deselect all objects in scene.
        for obj in bpy.context.selected_objects:
            obj.select_set(state=False)
        # Select all newly created objects.
        for obj in created_objects:
            obj.select_set(state=True)
        bpy.context.view_layer.objects.active = ob
        bpy.ops.object.join()
        bpy.context.object.name = self.name + '_balls_and_sticks'

        return bpy.context.object
   
    def draw_normal_mode(self, mode_idx=0, color='green', radius=0.02, units='nm', 
                   scale=1.0, join=True, with_H=True, subsurf_level=0,
                   segments=16):
        """Draw spheres for all atoms.

        Args: 
            color (str, ='by_element'): If 'by_element', uses colors in
                ELEMENT_COLORS. Otherwise, can specify color for whole
                model. Must be defined in COLORS.
            radius (float, =None): If specified, gives radius of all 
                atoms.
            units (str, ='nm'): Units for 1 BU. Can also be A.
            join (bool, =True): If true, all atoms are joined together
                into a single Bl object.
            with_H (bool, =True): Include the hydrogens.
            subsurf_level (int, =2): Subsurface subdivisions that will
                be applied to the atoms.
            segments (int, =16): Number of segments in each UV sphere
                primitive

        Returns:
            The atoms as a single Blender object, if join=True.
            Otherwise, None.
        """

        # Store start time to time script.
        start_time = time.time()

        # Holds links to all created objects, so that they can be
        # joined.
        created_objects = []

        # Initiate progress monitor over mouse cursor.
        bpy.context.window_manager.progress_begin(0, len(self.atoms))
        

        n = 0

        draw_thresh = 0.01
        for vec in self.normal_modes[mode_idx]:
            if np.linalg.norm( vec.vector * scale ) < draw_thresh:
                continue
            created_objects.append( vec.draw(scale = scale, radius=radius) )
            n += 1
            bpy.context.window_manager.progress_update(n)

        if n == 0:
            # no arrows were drawn! maybe there are all zeros on the mode vector?
            raise ValueError(f"No vectors drawn! All normal mode vectors are shorter than the drawing threshold ({draw_thresh} nm).")
        #n = 0
        #for a in self.atoms:
        #    if with_H or a.at_num != 1:
        #        created_objects.append(a.draw(color=color, radius=radius, 
        #                                      units=units, scale=scale,
        #                                      subsurf_level=subsurf_level,
        #                                      segments=segments))
        #    n += 1
        #    bpy.context.window_manager.progress_update(n)

        # End progress monitor.
        bpy.context.window_manager.progress_end()

        if join:
            # Deselect all objects in scene.
            for obj in bpy.context.selected_objects:
                obj.select_set(state=False)
            # Select all newly created objects.
            for obj in created_objects:
                obj.select_set(state=True)
            bpy.context.view_layer.objects.active = created_objects[0]
            bpy.ops.object.join()
            bpy.context.object.name = self.name + f'_mode_{mode_idx+1}'
            
        print("{} seconds".format(time.time()-start_time))
        return bpy.context.object



class ModeVector:
    """A vector describing the motion of a single atom in a normal mode.

    Attributes:
        atom: The atom, the vector is attached to
        vector: The actual vector
    """

    def __init__(self, atom, vector):
        self.atom = atom
        self.vector = vector
    
    def __repr__(self):
        outstr = f"ModeVector: atom_id = {self.atom.id_num}; type {self.atom.at_num}; vector = {self.vector}"
        return outstr

    @staticmethod
    def _draw_cone(location, rot_angle, rot_axis, 
                   radius=0.04, color='green', units='nm',
                   vertices=16, edge_split=False, start=0, end=0):
        """Draw the normal mode arrow head for this atom.

        Args:
            location (np.array): The center point of the half bond.
            length (float): The length of the half bond.
            rot_angle (float): Angle by which bond will be rotated.
            rot_axis (np.array): Axis of rotation.
            element (int): atomic number of element of the bond (for 
                coloring).
            radius (float, =0.2): radius of the bond.
            color (string, ='by_element'): color of the bond. If
                'by_element', uses element coloring.
            units (string, ='nm'): 1 BU = 1 nm, by default. Can change
                to angstroms ('A').
            vertices (int, =64): Number of vertices in each bond
                cylinder.
            edge_split (bool, =False): Whether to apply the edge split
                modifier to each bond.

        Returns:
            The new arrow (Blender object).
        """


        length = 1.618 * 2 * radius # golden ratio

        loc_corr = tuple(c*UNIT_CONV[units] for c in location)
        len_corr = length * UNIT_CONV[units]
        radius_corr = radius * UNIT_CONV[units]
        start = start * UNIT_CONV[units]
        end = end * UNIT_CONV[units]
        if True:
            dx = end[0] - start[0]
            dy = end[1] - start[1]
            dz = end[2] - start[2]
            dist = math.sqrt(dx**2 + dy**2 + dz**2)

            bpy.ops.mesh.primitive_cone_add(vertices=vertices,
                                            radius1=radius_corr, 
                                            radius2=0.,
                                            depth=len_corr, location=(dx/2+start[0], dy/2+start[1], dz/2+start[2]), 
                                            end_fill_type='NOTHING')            

            phi = math.atan2(dy, dx) 
            theta = math.acos(dz/dist) 

            bpy.context.object.rotation_euler[1] = theta 
            bpy.context.object.rotation_euler[2] = phi 
        
        bpy.ops.object.shade_smooth()

        if edge_split:
            bpy.ops.object.modifier_add(type='EDGE_SPLIT')
            bpy.ops.object.modifier_apply(modifier='EdgeSplit')

        bond_color = color

        if not bond_color in bpy.data.materials:
            _create_new_material(bond_color, COLORS[bond_color])

        bpy.context.object.data.materials.append(
            bpy.data.materials[bond_color])

        return bpy.context.object


    @staticmethod
    def _draw_cylinder(location, length, rot_angle, rot_axis, 
                   radius=0.02, color='green', units='nm',
                   vertices=16, edge_split=False, start=0, end=0):
        """Draw the normal mode arrow for this atom.

        Args:
            location (np.array): The center point of the half bond.
            length (float): The length of the half bond.
            rot_angle (float): Angle by which bond will be rotated.
            rot_axis (np.array): Axis of rotation.
            element (int): atomic number of element of the bond (for 
                coloring).
            radius (float, =0.2): radius of the bond.
            color (string, ='by_element'): color of the bond. If
                'by_element', uses element coloring.
            units (string, ='nm'): 1 BU = 1 nm, by default. Can change
                to angstroms ('A').
            vertices (int, =64): Number of vertices in each bond
                cylinder.
            edge_split (bool, =False): Whether to apply the edge split
                modifier to each bond.

        Returns:
            The new arrow (Blender object).
        """

        loc_corr = tuple(c*UNIT_CONV[units] for c in location)
        len_corr = length * UNIT_CONV[units]
        radius_corr = radius * UNIT_CONV[units]
        start = start * UNIT_CONV[units]
        end = end * UNIT_CONV[units]
        if True:
            dx = end[0] - start[0]
            dy = end[1] - start[1]
            dz = end[2] - start[2]
            dist = math.sqrt(dx**2 + dy**2 + dz**2)

            bpy.ops.mesh.primitive_cylinder_add(vertices=vertices,
                                            radius=radius_corr, 
                                            depth=len_corr, location=(dx/2+start[0], dy/2+start[1], dz/2+start[2]), 
                                            end_fill_type='NOTHING')            

            phi = math.atan2(dy, dx) 
            theta = math.acos(dz/dist) 

            bpy.context.object.rotation_euler[1] = theta 
            bpy.context.object.rotation_euler[2] = phi 
        
        bpy.ops.object.shade_smooth()

        if edge_split:
            bpy.ops.object.modifier_add(type='EDGE_SPLIT')
            bpy.ops.object.modifier_apply(modifier='EdgeSplit')

        bond_color = color

        if not bond_color in bpy.data.materials:
            _create_new_material(bond_color, COLORS[bond_color])

        bpy.context.object.data.materials.append(
            bpy.data.materials[bond_color])

        return bpy.context.object


    def draw(self, radius=0.02, scale=1., color='green', units='nm',
             vertices=64, edge_split=False):
        """Draw the arrow.

        Args:
            radius (float, =0.2): Radius of cylinder in angstroms.
            scale (float, =1.0): Length scale for the arrow.
            color (string, ='by_element'): Color of the bond. If
                'by_element', each half gets element coloring.
            units (string, ='nm'): 1 BU = 1 nm, by default. Can change
                to angstroms ('A').
            vertices (int, =64): Number of vertices in each bond
                cylinder.
            edge_split (bool, =False): Whether to apply the edge split
                modifier to each bond.

        Returns:
            The arrow (Blender object), with both halves joined.
        """

        created_objects = []

        center_loc = self.atom.location + scale * self.vector / 2
        end_loc = self.atom.location + scale * self.vector
        arrow_vector = self.vector * scale
        length = np.linalg.norm(arrow_vector)

        arrow_axis = arrow_vector/length # normalise length
        cyl_axis = np.array((0,0,1)) # z axis
        rot_axis = np.cross(arrow_axis, cyl_axis) # rotation from z-axis
        angle = -np.arccos(np.dot(cyl_axis, arrow_axis)) # calculate rotation angle

        #start_center = (self.atom1.location + center_loc)/2
        created_objects.append(ModeVector._draw_cylinder(center_loc, length, angle, 
                               rot_axis, radius, color, 
                               units, vertices, edge_split, self.atom.location, end_loc))
        
        cone_start = self.atom.location + scale * self.vector
        cone_center = self.atom.location + scale * 1.05 * self.vector 
        cone_end = self.atom.location + scale * 1.1 * self.vector 
        created_objects.append(ModeVector._draw_cone(cone_center, angle, 
                               rot_axis, 2*radius, color, 
                               units, vertices, edge_split, cone_start, cone_end))

        #end_center = (self.atom2.location + center_loc)/2
        #created_objects.append(Bond._draw_half(end_center, length/2, angle, 
        #                       rot_axis, self.atom2.at_num, radius, color, 
        #                       units, vertices, edge_split, center_loc, self.atom2.location))

        for obj in bpy.context.selected_objects:
            obj.select_set(state=False)

        for obj in created_objects:
            obj.select_set(state=True)

        bpy.ops.object.join()
        bpy.context.object.name = "arrow_{}({})".format(
            self.atom.id_num, self.atom.at_num
            )
        
        return bpy.context.object


class Volume:

    def __init__(self, name, offset, ng, basis, data=None):
        self.name = name
        self.offset = offset
        self.ng = ng
        self.basis = basis
        if data is None:
            self.data = np.zeros( tuple(ng) )
        else:
            self.data = data

    def __repr__(self):
        outstr = f"Volume {self.name}\nOffset:\n{self.offset}\nNumber of grid point:\n{self.ng}\nBasis:\n{self.basis[0]}\n{self.basis[1]}\n{self.basis[2]}"
        outstr += f"\nData value range: {np.amin(self.data)} to {np.amax(self.data)}"
        return outstr
        

    def draw_surface(self, name='surface', thresh=0.02, scale=True, color_cube=None, alpha=0.6, scale_colors=True, units='nm'):
        
        if self.data is None:
            print("This Volume object contains no data!")
            return

        b = np.array(self.basis)
        if scale:
            rel_thresh = thresh
            thresh = max( abs(np.amax(self.data)), abs(np.amin(self.data)) )
            thresh *= rel_thresh
        vertices, triangles = mcubes.marching_cubes(self.data, thresh)
        vertices = vertices.tolist()
        triangles = triangles.tolist()
        for i in range(len(vertices)):
            # move every point to correct position
            pos = (np.matmul(np.array(vertices[i]),b) + self.offset) * UNIT_CONV[units]
            vertices[i] = tuple(pos) #tuple(UNIT_CONV[units] * ( np.array(vertices[i]) / basis + offset ) )
        for i in range(len(triangles)):
            triangles[i] = tuple(triangles[i])
        
        # offset from octopus grid box relativ to cube located at origin in blender
        #loc = offset*UNIT_CONV[units] + r / 2.

        me = bpy.data.meshes.new(name)
        ob = bpy.data.objects.new(name, me)
        ob.location = bpy.context.scene.cursor.location
        bpy.context.collection.objects.link(ob)
        me.from_pydata(vertices, [], triangles)
        me.update(calc_edges=True)
        if thresh < 0.:
            me.flip_normals() #normals_make_consistent(inside=False)
        
        # set smooth shading
        bm = bmesh.new()
        bm.from_mesh(me)
        for f in bm.faces:
            f.smooth = True
        bm.to_mesh(me)
        bm.free()
        

        color_cube_ok = True
        if color_cube is not None:
            # check if basis of cube file is ok for interpolation
            for i in range(3):
                for j in range(3):
                    if i != j:
                        if np.abs(color_cube.basis[i][j]) > 1e-10:
                            color_cube_ok = False
        if not color_cube_ok:
            print("WARNING: Basis of color_cube cannot be interpolated!")
            print(color_cube.basis)
            print("Vertex paint will not be used")
            color_cube = None

        if color_cube is None:
            # set material
            mat = "SurfaceGreen"
            if not mat in bpy.data.materials:
                    _create_new_surface_material(mat, COLORS["green_surface"], alpha)
        
            ob.data.materials.append(bpy.data.materials[mat])
        else:
            # use colors of given cube file
            mat = "SurfaceVertexPaint"
            if not mat in bpy.data.materials:
                    _create_new_surfacevertexpaint_material(mat, COLORS["green_surface"], alpha)
            # create new material with vertex color node -> new function _create_new_vertexpaint_surface_material
            # edit vertex colors of obj
            # define color scale, get min and max values
            # 3d interpolate data of color_cube to obtain color at vertex positions
            # use scipy RegularGridInterpolator
            print("Interpolating cube to calculate colors at vertex positions")
            x_axis = np.array( [ color_cube.offset[0] + i*color_cube.basis[0][0] for i in range(color_cube.ng[0]) ] )
            y_axis = np.array( [ color_cube.offset[1] + i*color_cube.basis[1][1] for i in range(color_cube.ng[1]) ] )
            z_axis = np.array( [ color_cube.offset[2] + i*color_cube.basis[2][2] for i in range(color_cube.ng[2]) ] )
            
            interpolator = RegularGridInterpolator((x_axis, y_axis, z_axis), color_cube.data)
            
            cube_max = np.amax(color_cube.data)
            cube_min = np.amin(color_cube.data)

            if not me.vertex_colors:
                me.vertex_colors.new()
            color_layer = me.vertex_colors.active

            vert_idx = 0
            c_list = []
            for poly in me.polygons:
                for idx in poly.loop_indices:
                    
                    v_idx = me.loops[idx].vertex_index
                    pos = me.vertices[v_idx].co / UNIT_CONV[units] # vertex positions have to be converted back from BU to molecule units
                    c = interpolator( np.array( [ pos.x, pos.y, pos.z ] ) )[0]
                    
                    # convert interpolated value saved in c to color
                    # c is scaled to range of 0 to 1 taking. on the surface possibly not the full range is used
                    c = (c - cube_min) / (cube_max - cube_min)
                    c_list.append(c)
                    #print(v_idx, pos, c)
                    #if c < min_paint:
                    #    min_paint = c
                    #if c > max_paint:
                    #    max_paint = c

                    color_layer.data[vert_idx].color = jet_colormap(c)

                    vert_idx += 1

            if scale_colors:
                # go through all vertices and reassign colors so the full color scale range is used
                c_list = np.array(c_list)
                c_min = np.amin(c_list)
                c_max = np.amax(c_list)
                c_list = (c_list - c_min) / (c_max - c_min)

                vert_idx = 0
                for poly in me.polygons:
                    for idx in poly.loop_indices:
                        
                        v_idx = me.loops[idx].vertex_index
                        pos = me.vertices[v_idx].co / UNIT_CONV[units] # vertex positions have to be converted back from BU to molecule units
                        c = c_list[vert_idx] #interpolator( np.array( [ pos.x, pos.y, pos.z ] ) )[0]

                        color_layer.data[vert_idx].color = jet_colormap(c)

                        vert_idx += 1

 
            #print(cube_max, cube_min, min_paint, max_paint)

            ob.data.materials.append(bpy.data.materials[mat])

        return

    def draw_orbital(self, name="orbital", scale=True, thresh=0.02, alpha=0.7, units='nm'):
        """Draw two surfaces with positive and negative threshold each and combine them in one object."""
        
        if self.data is None:
            print("This Volume object contains no data!")
            return
        
        b = np.array(self.basis)
        
        if scale:
            rel_thresh = thresh
            thresh = max( abs(np.amax(self.data)), abs(np.amin(self.data)) )
            thresh *= rel_thresh
        
        vertices, triangles = mcubes.marching_cubes(self.data, thresh)
        vertices = vertices.tolist()
        triangles = triangles.tolist()
        for i in range(len(vertices)):
            # move every point to correct position
            pos = (np.matmul(np.array(vertices[i]),b) + self.offset) * UNIT_CONV[units]
            vertices[i] = tuple(pos) #tuple(UNIT_CONV[units] * ( np.array(vertices[i]) / basis + offset ) )
        for i in range(len(triangles)):
            triangles[i] = tuple(triangles[i])
        
        me = bpy.data.meshes.new("Mesh")
        ob = bpy.data.objects.new("Mesh", me)
        ob.location = bpy.context.scene.cursor.location
        bpy.context.collection.objects.link(ob)
        me.from_pydata(vertices, [], triangles)
        me.update(calc_edges=True)
        if thresh < 0.:
            me.flip_normals() #normals_make_consistent(inside=False)
        
        # set smooth shading
        bm = bmesh.new()
        bm.from_mesh(me)
        for f in bm.faces:
            f.smooth = True
        bm.to_mesh(me)
        bm.free()

        # set material
        mat = "SurfaceRed"
        if not mat in bpy.data.materials:
                _create_new_surface_material(mat, COLORS["red_surface"], alpha)
        
        ob.data.materials.append(bpy.data.materials[mat])

        # make second surface
        b = np.array(self.basis)
        vertices, triangles = mcubes.marching_cubes(self.data, -thresh)
        vertices = vertices.tolist()
        triangles = triangles.tolist()
        for i in range(len(vertices)):
            # move every point to correct position
            pos = (np.matmul(np.array(vertices[i]),b) + self.offset) * UNIT_CONV[units]
            vertices[i] = tuple(pos) #tuple(UNIT_CONV[units] * ( np.array(vertices[i]) / basis + offset ) )
        for i in range(len(triangles)):
            triangles[i] = tuple(triangles[i])
        
        me2 = bpy.data.meshes.new("Mesh")
        ob2 = bpy.data.objects.new("Mesh", me2)
        ob2.location = bpy.context.scene.cursor.location
        bpy.context.collection.objects.link(ob2)
        me2.from_pydata(vertices, [], triangles)
        me2.update(calc_edges=True)
        if -thresh < 0.:
            me2.flip_normals() #normals_make_consistent(inside=False)
        
        # set smooth shading
        bm = bmesh.new()
        bm.from_mesh(me2)
        for f in bm.faces:
            f.smooth = True
        bm.to_mesh(me2)
        bm.free()

        # set material
        mat = "SurfaceBlue"
        if not mat in bpy.data.materials:
                _create_new_surface_material(mat, COLORS["blue_surface"], alpha)
        
        ob2.data.materials.append(bpy.data.materials[mat])


        ### join both surfaces
        # Deselect all objects in scene.
        for obj in bpy.context.selected_objects:
            obj.select_set(state=False)
        # Select all newly created objects.
        for obj in (ob, ob2):
            obj.select_set(state=True)
        bpy.context.view_layer.objects.active = ob
        bpy.ops.object.join()
        bpy.context.object.name = name
     
        return

def draw_unit_cell(lattice_vectors, units=1., alpha=0.6):
    """Draw a crystal unit cell parallelepiped.

    Args:
        lattice_vectors: list of 3 3D vectors
        units (sting, ='nm'): 1 BU = 1 nm by default. Can also be
            set to angstroms.
        alpha: alpha transparency

    Returns:
        The blender object.
    """

    # The corrected location (i.e., scaled to units.)
    loc_corr = (0., 0., 0.) # place at world origin

    # Work out the sphere radius in BU.
    #if not radius:
    #    rad_adj = UNIT_CONV['A']*RADII[self.at_num]*UNIT_CONV[units]*scale
    #else:
    #    rad_adj = radius*UNIT_CONV[units]*scale

    # Create sphere as bmesh.
    bm = bmesh.new()
    bmesh.ops.create_cube(bm)
    
    for f in bm.faces:
        f.smooth = False
    
    bm.verts.ensure_lookup_table()
    
    # manually set position vertices
    bm.verts[0].co.xyz = Vector((0.,0.,0.))
    bm.verts[1].co.xyz = Vector(tuple(lattice_vectors[2]))
    bm.verts[2].co.xyz = Vector(tuple(lattice_vectors[1]))
    bm.verts[3].co.xyz = Vector(tuple(lattice_vectors[1] + lattice_vectors[2]))
    bm.verts[4].co.xyz = Vector(tuple(lattice_vectors[0]))
    bm.verts[5].co.xyz = Vector(tuple(lattice_vectors[0] + lattice_vectors[2]))
    bm.verts[6].co.xyz = Vector(tuple(lattice_vectors[0] + lattice_vectors[1]))
    bm.verts[7].co.xyz = Vector(tuple(lattice_vectors[0] + lattice_vectors[1] + lattice_vectors[2]))


    for i, vert in enumerate(bm.verts):
        print(i, vert.co.xyz)
    # Convert to mesh.
    me = bpy.data.meshes.new("Mesh")
    bm.to_mesh(me)
    bm.free()

    # Assign mesh to object and place in space.
    unit_cell = bpy.data.objects.new("Unit Cell", me)
    bpy.context.collection.objects.link(unit_cell)
    
    unit_cell.location = loc_corr

    mat = "SurfaceGreen"
    if not mat in bpy.data.materials:
        _create_new_surface_material(mat, COLORS["green_surface"], alpha)
    
    unit_cell.data.materials.append(bpy.data.materials[mat])

    return unit_cell



def draw_esp(mol, esp, units='nm'):

    print(f"Calculating and drawing electrstatic potential at van-der-Waals surface for molecule {mol.name}")
    print(f"ESP data taken from {esp.name}")

    vdw = mol.calculate_vdw(esp)

    vdw.draw_surface(name=f"ESP of {mol.name}", thresh=0.5, color_cube=esp, units=units)

    return vdw
 
def read_cif(filename, units='A', bond_guess=0.15, repetitions=[2,2,2]):
    """Loads cif file type (Crystallographic Information File)
    
    Args:
        filename (string): The target file.
    """
    
    print("Reading file", filename)
    with open(filename) as f:
        txt = f.readlines()

    scale = UNIT_CONV[units]

    lattice = np.zeros((3,))
    angles = np.zeros((3,))

    mol = Molecule(filename)

    lattice_vectors = None
    reduced_coordinates = []
    atom_counter = 1

    for i, line in enumerate(txt):
        if line.find('_cell_length_a') != -1:
            lattice[0] = float(line.split()[1]) * scale
        elif line.find('_cell_length_b') != -1:
            lattice[1] = float(line.split()[1]) * scale
        elif line.find('_cell_length_c') != -1:
            lattice[2] = float(line.split()[1]) * scale
        elif line.find('_cell_angle_alpha') != -1:
            angles[0] = float(line.split()[1])
        elif line.find('_cell_angle_beta') != -1:
            angles[1] = float(line.split()[1])
        elif line.find('_cell_angle_gamma') != -1:
            angles[2] = float(line.split()[1])
        else:
            line = line.split()
            if len(line) == 7 and line[0].isalpha(): # atom description
                if lattice_vectors == None: # calculate only once the lattice vectors
                    # http://gisaxs.com/index.php/Unit_cell
                    lattice_vectors = [ np.zeros( (3,) ) for xyz in range(3) ]
                    # ( a, 0, 0 )
                    lattice_vectors[0][0] = lattice[0]
                    # (b * cos(gamma), b*sin(gamma), 0 )
                    lattice_vectors[1][0] = lattice[1] * np.cos(np.deg2rad(angles[2]))
                    lattice_vectors[1][1] = lattice[1] * np.sin(np.deg2rad(angles[2]))

                    # c axis is more complicated
                    lattice_vectors[2][0] = lattice[2] * np.cos(np.deg2rad(angles[1]))
                    lattice_vectors[2][1] = lattice[2] * (np.cos(np.deg2rad(angles[0])) - np.cos(np.deg2rad(angles[1])) * np.cos(np.deg2rad(angles[2]))) / np.sin(np.deg2rad(angles[2]))
                    lattice_vectors[2][2] = lattice[2] * (
                                                           1 - np.cos(np.deg2rad(angles[1]))**2
                                                           - ( np.cos(np.deg2rad(angles[0])) - np.cos(np.deg2rad(angles[1])) * np.cos(np.deg2rad(angles[2])))
                                                             / np.sin(np.deg2rad(angles[2]) )**2
                                                         )**0.5

                
                atnum = ATOMIC_NUMBERS[line[0].upper()]
                reduced = np.array([ float(line[3+abc]) for abc in range(3) ])
                reduced_coordinates.append(reduced)
                for x in range(repetitions[0]):
                    for y in range(repetitions[1]):
                        for z in range(repetitions[2]):
                            # calculate position in cartesian coordinates
                            coords = (  (x+reduced[0]) * lattice_vectors[0]
                                      + (y+reduced[1]) * lattice_vectors[1]
                                      + (z+reduced[2]) * lattice_vectors[2] )
                            mol.add_atom(Atom(atnum, coords, atom_counter))
                            atom_counter += 1


    print(f"Got {atom_counter-1} atoms from {repetitions} repetitions")
    print("Lattice parameters (nm)")
    print(lattice)
    print("Angles")
    print(angles)
    print("Lattice vectors")
    print(lattice_vectors)
    
    mol.guess_bonds(threshold=bond_guess)

    return mol, lattice_vectors # offset, ng, basis, data

def read_orca(filename, units='A', bond_guess=0.15):
    """Loads an Orca output file.
    Currently tested for Orca 4.2.1

    Args:
        filename (string): The target file.
"""
    print("Reading file", filename)
    with open(filename) as f:
        txt = f.readlines()

    scale = UNIT_CONV[units]

    mol = Molecule(filename)


    has_read_normal_modes = False

    for i in range(len(txt)-1, -1, -1): # go from back to start to read last geom e.g. of geom opt
        line = txt[i]
        if line.find('CARTESIAN COORDINATES (ANGSTROEM)') != -1:
            offset = 2
            counter = 1

            while True:
                tmp = txt[i+offset].split()
                if len(tmp) != 4:
                    break
                atnum = ATOMIC_NUMBERS[ tmp[0].upper() ]
                coords = scale * np.array( [ float(tmp[1+xyz]) for xyz in range(3) ] )
                print(coords)
                mol.add_atom(Atom( atnum, coords, counter))
                counter += 1
                offset += 1

            # get number of atoms:
            n_atoms = len(mol.atoms)
            
            # initialize normal modes list with maximum number of modes
            normal_modes = [ [] for j in range(3*n_atoms) ]
            
            break # read only 1 geom

    for i, line in enumerate(txt):
        if line.find('NORMAL MODES') != -1:
            # got description of normal coordinates
            # there are up to 6 modes next to each other
            n_modes = 3 * n_atoms
            offset = 8
            block_size = n_modes + 1
            for j in range(n_modes):
                block = j // 6
                column = j % 6
                mode_idx = j
                for a in range(n_atoms):
                    vec = np.zeros((3,))
                    for xyz in range(3):
                        try:
                            vec[xyz] = float( txt[i+offset+block*block_size+3*a+xyz].split()[1+column] )
                        except IndexError:
                            print('IndexError')
                            print(txt[i+offset+block*block_size+3*a+xyz])
                            print(j, a, xyz, column, block)
                            raise
                    normal_modes[mode_idx].append( ModeVector( mol.atoms[a], vec) )
            break
    
    if len(normal_modes[0]) > 0:
        has_read_normal_modes = True


    # save normal modes in mol object
    if has_read_normal_modes:
        # check for number of normal modes
        final_normal_modes = []
        for mode in normal_modes:
            if len(mode) > 0:
                final_normal_modes.append(mode)
        mol.normal_modes = final_normal_modes


    mol.guess_bonds(threshold=bond_guess)
    return mol

def read_log(filename, units='A', bond_guess=0.15):
    """Loads Gaussian log file
    
    Args:
        filename (string): The target file.
    """
    
    print("Reading file", filename)
    with open(filename) as f:
        txt = f.readlines()

    scale = UNIT_CONV[units]

    mol = Molecule(filename)


    has_read_normal_modes = False

    for i in range(len(txt)-1, -1, -1): # go from back to start to read last geom e.g. of geom opt
        line = txt[i]
        if line.find('Input orientation') != -1:
            offset = 5
            counter = 1

            while True:
                tmp = txt[i+offset].split()
                if len(tmp) != 6:
                    break
                atnum = int(tmp[1])
                coords = scale * np.array( [ float(tmp[3+xyz]) for xyz in range(3) ] )
                print(coords)
                mol.add_atom(Atom( atnum, coords, counter))
                counter += 1
                offset += 1

            # get number of atoms:
            n_atoms = len(mol.atoms)
            
            # initialize normal modes list
            normal_modes = [ [] for j in range(3*n_atoms) ]
            
            break # read only 1 geom


    for i, line in enumerate(txt):
        if line.find('Coord Atom Element:') != -1:
            # got description of normal coordinates
            # there are up to 5 modes next to each other
            n_modes = len(txt[i-9].split())
            for j in range(n_modes):
                # get index of mode
                mode_idx = int(txt[i-9].split()[j]) - 1
                for a in range(n_atoms):
                    vec = np.zeros((3,))
                    for xyz in range(3):
                        vec[xyz] = float(txt[i+1+3*a+xyz].split()[3+j])
                    normal_modes[mode_idx].append( ModeVector( mol.atoms[a], vec ) )
    
    if len(normal_modes[0]) > 0:
        has_read_normal_modes = True


    for i, line in enumerate(txt): # check normal modes in other format
        if has_read_normal_modes:
            break # not necessary if normal modes have already been read

        if line.find('Atom  AN      X      Y      Z') != -1:
            # got description of normal coordinates
            # there are up to 3 modes next to each other
            n_modes = len(txt[i-6].split())
            for j in range(n_modes):
                # get index of mode
                mode_idx = int(txt[i-6].split()[j]) - 1
                for a in range(n_atoms):
                    vec = np.zeros((3,))
                    for xyz in range(3):
                        vec[xyz] = float(txt[i+1+a].split()[2+3*j+xyz])
                    normal_modes[mode_idx].append( ModeVector( mol.atoms[a], vec ) )

    
    if len(normal_modes[0]) > 0:
        has_read_normal_modes = True

    # save normal modes in mol object
    if has_read_normal_modes:
        # check for number of normal modes
        final_normal_modes = []
        for mode in normal_modes:
            if len(mode) > 0:
                final_normal_modes.append(mode)
        mol.normal_modes = final_normal_modes

    mol.guess_bonds(threshold=bond_guess)

    return mol # offset, ng, basis, data

def read_coord(filename, units='A', bond_guess=0.15):
    """Loads turbomole coord file
    
    Args:
        filename (string): The target file.
    """
    
    print("Reading file", filename)
    with open(filename) as f:
        txt = f.readlines()

    scale = UNIT_CONV[units]

    # read molecule data
    #natom = int(txt[0])
    #print("Got %i atoms" % natom)
    mol = Molecule(filename)
    i = 0
    for line in txt: #i in range(natom): #line in enumerate(txt[6:]):
        if line.find('$') != -1:
            continue
        line = line.split()
        atnum = ATOMIC_NUMBERS[line[3].upper()]
        coords = scale * np.array( [ float(line[xyz]) for xyz in range(3) ] )
        print(coords)
        mol.add_atom(Atom(atnum, coords, i+1))
        i += 1
    
    mol.guess_bonds(threshold=bond_guess)

    print(f"Got {i} atoms")
    
    return mol # offset, ng, basis, data


   
def read_xyz(filename, units='A', bond_guess=0.15):
    """Loads xyz file
    
    Args:
        filename (string): The target file.
    """
    
    print("Reading file", filename)
    with open(filename) as f:
        txt = f.readlines()

    scale = UNIT_CONV[units]

    # read molecule data
    natom = int(txt[0])
    print("Got %i atoms" % natom)
    mol = Molecule(filename)
    for i in range(natom): #line in enumerate(txt[6:]):
        line = txt[2+i].split()
        atnum = ATOMIC_NUMBERS[line[0].upper()] #int(line[0])
        coords = scale * np.array( [ float(line[1+xyz]) for xyz in range(3) ] )
        print(coords)
        mol.add_atom(Atom(atnum, coords, i+1))
    
    mol.guess_bonds(threshold=bond_guess)

    return mol # offset, ng, basis, data

def read_cube(filename, units='A', bond_guess=0.15):
    """Loads octopus cube file
    
    Args:
        filename (string): The target file.
    """
    
    print("Reading file", filename)
    with open(filename) as f:
        txt = f.readlines()

    scale = UNIT_CONV[units]

    offset = scale*np.array( [ float(txt[2].split()[1]), float(txt[2].split()[2]), float(txt[2].split()[3]) ] )
    print("Grid offset: ", offset)
    ng = [ abs(int(txt[3].split()[0])), abs(int(txt[4].split()[0])), abs(int(txt[5].split()[0])) ]
    print("Grid size: ", ng[0], ng[1], ng[2])

    basis = [ scale * np.array( [float(txt[3+abc].split()[1+xyz]) for xyz in range(3)] ) for abc in range(3) ]
    basis = np.array(basis)

    print("Basis vectors for grid are:")
    for abc in range(3):
        print(basis[abc])

    # read molecule data
    natom = abs( int(txt[2].split()[0]) )
    print("Got %i atoms" % natom)
    mol = Molecule(filename)
    for i in range(natom): #line in enumerate(txt[6:]):
        line = txt[6+i].split()
        atnum = int(line[0])
        coords = scale * np.array( [ float(line[2+xyz]) for xyz in range(3) ] )
        mol.add_atom(Atom(atnum, coords, i+1))
    
    mol.guess_bonds(threshold=bond_guess)

    # read volumetric data
    data =[] # np.zeros(ng)
    for line in txt[6+natom:]:
        line = line.split()
        for element in line:
            data.append(float(element))
    data = np.array( data )
    try:
        data.shape = tuple(ng)
    except ValueError:
        # Orca prints additional line with 2 numbers before the actual volume data
        data = data[2:]
        data.shape = tuple(ng)
    
    vol = Volume(filename, offset, ng, basis, data)

    return mol, vol # offset, ng, basis, data

def read_pdb(filename, units='A'):
    """Loads a pdb file into a molecule object. Only accepts atoms
    with Cartesian coords through the ATOM/HETATM label and bonds
    through the CONECT label.

    Args:
        filename (string): The target file.
    """
    
    mol = Molecule(filename)
    
    scale = UNIT_CONV[units]

    with open(filename) as pdbfile:
        for line in pdbfile:
            if line[0:4] == "ATOM":
                idnum = int(line[6:11])
                atnum = ATOMIC_NUMBERS[line[76:78].strip().upper()]
                coords = scale*np.array((float(line[30:38]), float(line[38:46]), 
                                   float(line[46:54])))
                mol.add_atom(Atom(atnum, coords, idnum))

            elif line[0:6] == "HETATM":
                idnum = int(line[6:11])
                atnum = ATOMIC_NUMBERS[line[76:78].strip().upper()]
                coords = scale*np.array((float(line[30:38]), float(line[38:46]), 
                                   float(line[46:54])))
                mol.add_atom(Atom(atnum, coords, idnum))

            elif line[0:6] == "CONECT":

                # Loads atoms as a list. First atom is bonded to the
                # remaining atoms (up to four).
                atoms = line[6:].split()
                for bonded_atom in atoms[1:]:
                    # print(atoms[0], bonded_atom)
                    mol.add_bond(int(atoms[0]), int(bonded_atom))

    return mol

def read_molden(filename, bond_guess=0.15):
    """Load data from molden file"""
    # read geometry
    # read AOs and create CGFs
    # read MO coefficients. Every MO should have 1 coefficient per CGF

    # return Molecule object with structure
    # return Molden object with orbitals

    # class Molden -> contains AOs and MOs, implements __repr__ and draw_orbital function
    # draw orbital calculates volume from selected MO's coeffs and calls draw_orbital of volume


    mol = Molecule(filename)
    ao_index_zero = False # sometimes AO indices start from 0, sometimes from 1
    
    print("opening file", filename)
    with open(filename) as f:
        txt = f.readlines()

    for i, line in enumerate(txt):
        if line.lower().find('[n_atoms]') != -1:
            natoms = int(txt[i+1])
        if line.lower().find('[atoms]') != -1:
            # read molecule geometry
            units = line.split()[1]
            if units == '(AU)':
                scale = UNIT_CONV['a.u.'] #Ang2B
            else:
                scale = UNIT_CONV['A']
            
            print("scale is", units)
            j = 0
            while True: #for j in range(natoms):
                tmp = txt[i+j+1]
                if tmp.find('[') != -1:
                    break
                tmp = tmp.split()
                atnum = int(tmp[2])
                coords = np.array( [ scale*float( tmp[3+xyz] ) for xyz in range(3) ] )
                mol.add_atom(Atom(atnum, coords, j+1))
                j += 1
            natoms = len(mol.atoms)
            print(f"Got {natoms} atoms")

            mol.guess_bonds(threshold=bond_guess)
        if line.lower().find('[mo]') != -1:
            print("Entering MO section")
            mo_list = []
            mo_counter = 0
            mo_length = 4 + len(ao_list) # number of lines that define 1 MO
            
            while True:
                try:
                    sym = txt[i+mo_counter*mo_length+1].split('=')[1].strip()
                except IndexError:
                    break # the last valid MO was read
                ene = float(txt[i+mo_counter*mo_length+2].split('=')[1].strip())
                spin = txt[i+mo_counter*mo_length+3].split('=')[1].strip()
                occup = float(txt[i+mo_counter*mo_length+4].split('=')[1].strip())
                
                mo_coeffs = [0. for i in range(len(ao_list))]
                for m in range(len(ao_list)):
                    mo_idx = int(txt[i+mo_counter*mo_length+5+m].split()[0]) - 1
                    mo_coeffs[mo_idx] = float(txt[i+mo_counter*mo_length+5+m].split()[1])
                    #mo_coeffs.append( float(txt[i+mo_counter*mo_length+5+m].split()[1]) )

                
                mo = {'sym': sym,
                      'ene': ene,
                      'spin': spin,
                      'occup': occup,
                      'coeffs': np.array(mo_coeffs) }

                print(mo)
                mo_list.append( mo )
                mo_counter += 1
            
            print(f"Got {len(mo_list)} MOs")

        if line.lower().find('[sto]') != -1:
            print("Entering STO section")
            ao_list = []
            
            j = 2 # line offset
            
            while True:
                tmp = txt[i+j].split()

                if len(tmp) != 8:
                    break # end of STO definition
                
                if int(tmp[7]) == 0:
                    ao_index_zero = True
                    
                active_atom = int(tmp[0]) - 1
                kx = int(tmp[1])
                ky = int(tmp[2])
                kz = int(tmp[3])
                kr = int(tmp[4])
                alpha = float(tmp[5])
                bnorm = float(tmp[6])
                orb_pos = mol.atoms[active_atom].location
                
                ao_list.append( STO(orb_pos, kx, ky, kz, kr, alpha, bnorm) )
                
                j += 1
            
            print(f"Got a total of {len(ao_list)} AOs")
            for i, ao in enumerate(ao_list):
                print(i+1, ao)                   
                
        
        if line.lower().find('[gto]') != -1:
            print("Entering GTO section")
            # reading AO info of using Gaussian functions
            ao_list = []
            
            supported_AOs = ('s', 'p', 'sp', 'd')

            j = 1 # line offset that is currently read
            active_atom = int(txt[i+j].split()[0]) - 1 # active atom
            shell_counter = 0
            print("Active atom is", active_atom)
            j += 1

            while True:
                tmp = txt[i+j].split()
                if len(tmp) == 0:
                    # go to next atom
                    active_atom += 1
                    if active_atom >= natoms: # note that active_atom starts counting at 0 (not like in molden file)
                        break
                    print("Active atom is", active_atom)
                    # set counter of principal quantum number to 0, assuming basis set starts with 1s
                    shell_counter = 0
                    j += 1
                    continue
                
                if tmp[0].isalpha(): # first element in tmp is letter
                    # reading new orbital type
                    shell = tmp[0].lower() # s, p, sp, ...
                    if shell in ('s', 'sp'):
                        shell_counter += 1
                    n_orbs = int(tmp[1]) # contracted GTO is constructed from n_orbs primitives
                    print(f"Got shell {shell_counter}{shell} of {n_orbs} primitives")
                    orb_pos = mol.atoms[active_atom].location
                    
                    alphas = []
                    coeffs = []
                    coeffs2 = []
                    
                    for k in range(n_orbs):
                            alphas.append( float(txt[i+j+k+1].split()[0]) )
                            coeffs.append( float(txt[i+j+k+1].split()[1]) )
                            if shell == 'sp':
                                coeffs2.append( float(txt[i+j+k+1].split()[2]) )
                    
                    if shell == 's':
                        ao_list.append( CGF( orb_pos, shell_counter, 's', alphas, coeffs ) )
                    elif shell == 'p':
                        ao_list.append( CGF( orb_pos, shell_counter, 'px', alphas, coeffs ) )
                        ao_list.append( CGF( orb_pos, shell_counter, 'py', alphas, coeffs ) )
                        ao_list.append( CGF( orb_pos, shell_counter, 'pz', alphas, coeffs ) )
                    elif shell == 'sp':
                        ao_list.append( CGF( orb_pos, shell_counter, 's', alphas, coeffs ) )
                        ao_list.append( CGF( orb_pos, shell_counter, 'px', alphas, coeffs2 ) )
                        ao_list.append( CGF( orb_pos, shell_counter, 'py', alphas, coeffs2 ) )
                        ao_list.append( CGF( orb_pos, shell_counter, 'pz', alphas, coeffs2 ) )
                    elif shell == 'd':
                        ao_list.append( CGF( orb_pos, shell_counter, 'd 0', alphas, coeffs) )
                        ao_list.append( CGF( orb_pos, shell_counter, 'd 1', alphas, coeffs) )
                        ao_list.append( CGF( orb_pos, shell_counter, 'd-1', alphas, coeffs) )
                        ao_list.append( CGF( orb_pos, shell_counter, 'd 2', alphas, coeffs) )
                        ao_list.append( CGF( orb_pos, shell_counter, 'd-2', alphas, coeffs) )
                                                                                                
                j += 1 # move to next line

            print(f"Got a total of {len(ao_list)} AOs")
            for i, ao in enumerate(ao_list):
                print(i+1, ao)
    
    molden = MoldenOrbitals(filename, ao_list, mo_list)
    return mol, molden


### taken from: https://blender.stackexchange.com/questions/57306/how-to-create-a-custom-ui


# ------------------------------------------------------------------------
#    Scene Properties
# ------------------------------------------------------------------------



class MyProperties(PropertyGroup):

    file_path: StringProperty(
        name = "Path",
        description="Choose a file",
        default="",
        maxlen=1024,
        subtype='FILE_PATH'
        )
    
    info_text: StringProperty(
        name = "Info",
        description="File information",
        default="",
        maxlen=4096,
        )

    bond_guess: FloatProperty(
        name = "Bond Guess Threshold",
        description = "Distance between atoms to draw bonds (unit is nm). Bonds including H use 75% of this value only.",
        default = 0.2,
        min = 0.0,
        max = 1.0
        )

    sphere_resolution: IntProperty(
        name = "Sphere Resolution",
        description = "Resolution of atom spheres",
        default = 16,
        min = 4,
        max = 128
        )
 
    repetition_a: IntProperty(
        name = "A Repetitions",
        description = "Crystal structure repetitions along lattice vector A.",
        default = 2,
        min = 1,
        max = 64
        )
    repetition_b: IntProperty(
        name = "B Repetitions",
        description = "Crystal structure repetitions along lattice vector B.",
        default = 2,
        min = 1,
        max = 64
        )
    repetition_c: IntProperty(
        name = "C Repetitions",
        description = "Crystal structure repetitions along lattice vector C.",
        default = 2,
        min = 1,
        max = 64
        )
    mode_idx: IntProperty(
        name = "Normal Mode",
        description = "Index of normal mode to draw.",
        default = 0,
        min = 0,
        max = 1024
        )
    mode_scale: FloatProperty(
        name = "Scale",
        description = "Scaling factor for length of normal mode vectors.",
        default = 0.25,
        min = -10.,
        max = 10.
        )
    mode_thickness: FloatProperty(
        name = "Radius",
        description = "Thickness/Radius of normal mode vectors.",
        default = 0.01,
        min = 0.,
        max = 1.
        )
 


    scale_density: BoolProperty(
        name="Scale Density",
        description="Scale density to maximum absolute of 1.0",
        default = True
        )
 
    draw_h: BoolProperty(
        name="Draw Hydrogen",
        description="Trigger drawing of hydrogen atoms",
        default = True
        )
            
    iso_thresh: FloatProperty(
        name = "Threshold",
        description = "Isosurface threshold",
        default = 0.02,
        min = -10.0,
        max = 10.0
        )

    coeff_thresh: FloatProperty(
        name = "MO Threshold",
        description = "Defines the MO coefficient that AOs need to have to contribute to drawing of orbital wave functions. 0 is most accurate. Increasing the value increases speed of surface calculations.",
        default = 0.1,
        min = 0.0,
        max = 1.0
        )

    mo_index: IntProperty(
        name = "Orbital Index",
        description = "Molden Orbital Index",
        default = 0,
        min = 0,
        max = 1024
        )
        
    mo_or_ao: BoolProperty(
        name="MO",
        description="If true, draw MOs, otherwise draw AOs",
        default = True
        )
        
    diffuse: BoolProperty(
        name="Diffuse",
        description="If false, AOs only contribute to wavefunctions 5 Angstrom around there center. This increases speed of surface calculations. Set to true in case you have diffuse functions or Rydberg orbitals.",
        default = False
        )     

    orbital_resolution: IntProperty(
        name = "Surface/Orbital Resolution",
        description = "Affects drawing quality of molden orbitals and vdW surfaces",
        default = 25,
        min = 4,
        max = 256
        )

    structure_type: EnumProperty(
        name="Structure",
        description="Type of structure to draw.",
        items=[ ('BS', "Balls and Sticks", "Simple balls and sticks model."),
                ('B', "Spheres", "Draws spheres with van-der-Waals radius."),
                ('S', "Sticks", "Stick model."),
               ],
        default = "BS"
        )

    file_units: EnumProperty(
        name="File Units",
        description="File units",
        items=[ ('A', "Angstrom", ""),
                ('a.u.', "Bohr (a.u.)", ""),
               ],
        default = "a.u."
        )

# ------------------------------------------------------------------------
#    Operators
# ------------------------------------------------------------------------

class WM_OT_AddMolecule(Operator):
    """Draw molecular structure."""
    
    bl_label = "Draw Molecule"
    bl_idname = "wm.add_molecule"

    def execute(self, context):
        scene = context.scene
        mytool = scene.my_tool

        mytool.file_path = bpy.path.abspath(mytool.file_path)

        if mytool.file_path.endswith('xyz'):
            mol = read_xyz(mytool.file_path, units=mytool.file_units, bond_guess=mytool.bond_guess)
            mytool.info_text = f"File contains {len(mol.atoms)} atoms"
        elif mytool.file_path.endswith('log'):
            mol = read_log(mytool.file_path, units=mytool.file_units, bond_guess=mytool.bond_guess)
            mytool.info_text = f"File contains {len(mol.atoms)} atoms"
            if mol.normal_modes is not None:
                mytool.info_text += f" and {len(mol.normal_modes)} normal modes"
        elif mytool.file_path.endswith('orca'):
            mol = read_orca(mytool.file_path, units=mytool.file_units)
            mytool.info_text = f"File contains {len(mol.atoms)} atoms"
            if mol.normal_modes is not None:
                mytool.info_text += f" and {len(mol.normal_modes)} normal modes"
 
        elif mytool.file_path.endswith('pdb'):
            mol = read_pdb(mytool.file_path, units=mytool.file_units)
            mytool.info_text = f"File contains {len(mol.atoms)} atoms"
        elif mytool.file_path.endswith('molden'):
            mol, molden = read_molden(mytool.file_path, bond_guess=mytool.bond_guess)
            mytool.info_text = f"File contains {len(mol.atoms)} atoms\n\n{molden}"
        elif  mytool.file_path.endswith('cube') or mytool.file_path.endswith('cub'):
            mol, vol = read_cube(mytool.file_path, units=mytool.file_units, bond_guess=mytool.bond_guess)
            mytool.info_text = f"File contains {len(mol.atoms)} atoms.\n\n{vol}"
        elif  mytool.file_path.endswith('cif'):
            repetitions = [ mytool.repetition_a, mytool.repetition_b, mytool.repetition_c ]
            mol, lattice_vectors = read_cif(mytool.file_path, units=mytool.file_units, bond_guess=mytool.bond_guess, repetitions=repetitions)
            mytool.info_text = f"Created structure contains {len(mol.atoms)} atoms.\nObtained from {repetitions} repetitions of unit cell"
        elif mytool.file_path.endswith('coord'):
            mol = read_coord(mytool.file_path, units=mytool.file_units, bond_guess=mytool.bond_guess)
            mytool.info_text = f"File contains {len(mol.atoms)} atoms"

        if mytool.structure_type == 'BS':
            mol.draw_bs(with_H=mytool.draw_h, segments=mytool.sphere_resolution)
        elif mytool.structure_type == 'B':
            mol.draw_atoms(with_H=mytool.draw_h, segments=mytool.sphere_resolution)
        elif mytool.structure_type == 'S':
            mol.draw_bonds(with_H=mytool.draw_h, vertices=mytool.sphere_resolution)
            
        

        return {'FINISHED'}


class WM_OT_AddUnitCell(Operator):
    """Creates a parallelepiped for the crystal unit cell."""
    
    bl_label = "Draw Unit Cell"
    bl_idname = "wm.add_unit_cell"

    def execute(self, context):
        scene = context.scene
        mytool = scene.my_tool
        # python needs absolute file paths, not relative to blender file
        mytool.file_path = bpy.path.abspath(mytool.file_path)

        if  mytool.file_path.endswith('cif'):
            repetitions = [ mytool.repetition_a, mytool.repetition_b, mytool.repetition_c ]
            mol, lattice_vectors = read_cif(mytool.file_path, units=mytool.file_units, bond_guess=mytool.bond_guess, repetitions=repetitions)
        else:
            self.report({"WARNING"}, "Only cif files supported for this operation.")
            return {"CANCELLED"}
            
        draw_unit_cell(lattice_vectors)

        return {'FINISHED'}

class WM_OT_AddESPSurface(Operator):
    """Creates a surface at the van-der-Waals radius around the atoms
    and uses vertex painting to color it according to cube volume data."""
    
    bl_label = "Draw Colored Surface"
    bl_idname = "wm.add_esp_surface"

    def execute(self, context):
        scene = context.scene
        mytool = scene.my_tool
        # python needs absolute file paths, not relative to blender file
        mytool.file_path = bpy.path.abspath(mytool.file_path)

        if  mytool.file_path.endswith('cube') or mytool.file_path.endswith('cub'):
            mol, vol = read_cube(mytool.file_path, units=mytool.file_units)
            mytool.info_text = f"File contains {len(mol.atoms)} atoms.\n\n{vol}"
        else:
            self.report({"WARNING"}, "Only cube files supported for this operation.")
            return {"CANCELLED"}
            
        draw_esp(mol, vol)

        return {'FINISHED'}


class WM_OT_AddVDWSurface(Operator):
    """Creates a surface at the van-der-Waals radius around the atoms."""
    
    bl_label = "Draw vdW Surface"
    bl_idname = "wm.add_vdw_surface"

    def execute(self, context):
        scene = context.scene
        mytool = scene.my_tool
        mytool.file_path = bpy.path.abspath(mytool.file_path)

        if mytool.file_path.endswith('xyz'):
            mol = read_xyz(mytool.file_path, units=mytool.file_units)
            mytool.info_text = f"File contains {len(mol.atoms)} atoms"
        elif mytool.file_path.endswith('molden'):
            mol, molden = read_molden(mytool.file_path)
            mytool.info_text = f"File contains {len(mol.atoms)} atoms"
        elif  mytool.file_path.endswith('cube') or mytool.file_path.endswith('cub'):
            mol, vol = read_cube(mytool.file_path, units=mytool.file_units)
            mytool.info_text = f"File contains {len(mol.atoms)} atoms.\n\n{vol}"
        elif mytool.file_path.endswith('coord'):
            mol = read_coord(mytool.file_path, units=mytool.file_units, bond_guess=mytool.bond_guess)
            mytool.info_text = f"File contains {len(mol.atoms)} atoms"
            
        mol.draw_vdw(points=mytool.orbital_resolution)

        return {'FINISHED'}


        # print the values to the console
        #print("Hello World")
        #print("bool state:", mytool.my_bool)
        #print("int value:", mytool.my_int)
        #print("float value:", mytool.my_float)
        #print("string value:", mytool.my_string)
        #print("enum state:", mytool.my_enum)

        #return {'FINISHED'}
        
class WM_OT_AddSurface(Operator):
    """Create surface from cube or molden data at the defined threshold."""
    
    bl_label = "Draw Surface"
    bl_idname = "wm.add_surface"

    def execute(self, context):
        scene = context.scene
        mytool = scene.my_tool
        mytool.file_path = bpy.path.abspath(mytool.file_path)

        if mytool.file_path.endswith('xyz'):
            self.report({"WARNING"}, "XYZ files do not contain surface data.")
            return {"CANCELLED"}
            #mol = read_xyz(mytool.file_path, units=mytool.file_units)
        elif mytool.file_path.endswith('cube') or mytool.file_path.endswith('cub'):
            mol, vol = read_cube(mytool.file_path, units=mytool.file_units)
            mytool.info_text = f"File contains {len(mol.atoms)} atoms.\n\n{vol}"
        else:
            self.report({"WARNING"}, "This function is only implemented for cube files.")
            return {"CANCELLED"}
        
        vol.draw_surface(name='surface', thresh=mytool.iso_thresh, scale=mytool.scale_density)

        return {'FINISHED'}

class WM_OT_AddOrbital(Operator):
    """Create orbital surface from cube or molden data at +/- threshold."""
    
    bl_label = "Draw Orbital"
    bl_idname = "wm.add_orbital"

    def execute(self, context):
        scene = context.scene
        mytool = scene.my_tool
        mytool.file_path = bpy.path.abspath(mytool.file_path)
        
        if mytool.file_path.endswith('xyz'):
            self.report({"WARNING"}, "XYZ files do not contain surface data.")
            return {"CANCELLED"}
        elif mytool.file_path.endswith('cube') or mytool.file_path.endswith('cub'):
            mol, vol = read_cube(mytool.file_path, units=mytool.file_units)
            vol.draw_orbital(name='orbital', thresh=mytool.iso_thresh, scale=mytool.scale_density)
            mytool.info_text = f"File contains {len(mol.atoms)} atoms.\n\n{vol}"
        elif mytool.file_path.endswith('molden'):
            mol, molden = read_molden(mytool.file_path)

            molden.draw_orbital(name='orbital', index=mytool.mo_index,
                                thresh=mytool.iso_thresh,
                                mo=mytool.mo_or_ao,
                                resolution=mytool.orbital_resolution,
                                coeff_thresh=mytool.coeff_thresh,
                                diffuse=mytool.diffuse)

        return {'FINISHED'}

class WM_OT_AddModes(Operator):
    """Create 3D arrows for normal mode vectors."""
    
    bl_label = "Draw Modes"
    bl_idname = "wm.add_modes"

    def execute(self, context):
        scene = context.scene
        mytool = scene.my_tool
        mytool.file_path = bpy.path.abspath(mytool.file_path)
        
        if not ( mytool.file_path.endswith('log') or mytool.file_path.endswith('orca') ):
            self.report({"WARNING"}, "Normal modes can only be read from log or orca output files.")
            return {"CANCELLED"}
        elif mytool.file_path.endswith('log'):
            mol = read_log(mytool.file_path, units=mytool.file_units, bond_guess=mytool.bond_guess)
            mytool.info_text = f"File contains {len(mol.atoms)} atoms"
            if mol.normal_modes is not None:
                mytool.info_text += f" and {len(mol.normal_modes)} normal modes"
            if mol.normal_modes is None:
                self.report({"WARNING"}, "Log file does not contain normal modes.")
                return {"CANCELLED"}
            elif mytool.mode_idx > len(mol.normal_modes):
                self.report({"WARNING"}, f"Normal mode index must be in range 1 to {len(mol.normal_modes)}.")
                return {"CANCELLED"}
            mol.draw_normal_mode(mode_idx = mytool.mode_idx-1, scale = mytool.mode_scale, radius = mytool.mode_thickness )
        elif mytool.file_path.endswith('orca'):
            mol = read_orca(mytool.file_path, units=mytool.file_units, bond_guess=mytool.bond_guess)
            mytool.info_text = f"File contains {len(mol.atoms)} atoms"
            if mol.normal_modes is not None:
                mytool.info_text += f" and {len(mol.normal_modes)} normal modes"
            if mol.normal_modes is None:
                self.report({"WARNING"}, "Orca output file does not contain normal modes.")
                return {"CANCELLED"}
            elif mytool.mode_idx > len(mol.normal_modes):
                self.report({"WARNING"}, f"Normal mode index must be in range 1 to {len(mol.normal_modes)}.")
                return {"CANCELLED"}
            try:
                mol.draw_normal_mode(mode_idx = mytool.mode_idx-1, scale = mytool.mode_scale, radius = mytool.mode_thickness )
            except ValueError as e:
                self.report({"WARNING"}, str(e))
                return {"CANCELLED"}

        return {'FINISHED'}


# ------------------------------------------------------------------------
#    Panel in Object Mode
# ------------------------------------------------------------------------

class OBJECT_PT_MoleculePanel(Panel):
    bl_label = "Molecule"
    bl_idname = "OBJECT_PT_molecule_panel"
    bl_space_type = "VIEW_3D"   
    bl_region_type = "UI"
    bl_category = "Molecule"
    bl_context = "objectmode"   


    #@classmethod
    #def poll(self,context):
    #    return context.object is not None

    def draw(self, context):
        layout = self.layout
        scene = context.scene
        mytool = scene.my_tool

        #layout.prop(mytool, "my_bool")
        layout.label(text="Input Data")
        layout.prop(mytool, "file_path")
        layout.prop(mytool, "file_units")
        
        layout.separator()
        layout.label(text="Structure Operations")
        layout.prop(mytool, "structure_type", text="") 

        layout.operator("wm.add_molecule")
        row = layout.row()
        row.prop(mytool, "draw_h")
        row.prop(mytool, "bond_guess")
        row.prop(mytool, "sphere_resolution")
        row = layout.row()
        row.prop(mytool, "repetition_a")
        row.prop(mytool, "repetition_b")
        row.prop(mytool, "repetition_c")
        row = layout.row()
        row.operator("wm.add_unit_cell")
        row.operator("wm.add_modes")
        row = layout.row()
        row.prop(mytool, "mode_idx")
        row.prop(mytool, "mode_scale")
        row.prop(mytool, "mode_thickness")
        row = layout.row()
        row.prop(mytool, "scale_density")
        row.prop(mytool, "orbital_resolution")
        row.operator("wm.add_vdw_surface")
        #layout.menu(OBJECT_MT_CustomMenu.bl_idname, text="Presets", icon="SCENE")
        
        layout.separator()
        layout.label(text="Volume Operations")
        if mcubes_avail:
            layout.operator("wm.add_esp_surface")
            layout.operator("wm.add_surface")
            layout.operator("wm.add_orbital")
            layout.prop(mytool, "iso_thresh")

        layout.separator()
        layout.label(text="Molden Operations")
        layout.prop(mytool, "mo_index")
        row = layout.row()
        row.prop(mytool, "mo_or_ao")
        row.prop(mytool, "diffuse")
        row.prop(mytool, "coeff_thresh")
        
        
        layout.separator()
        #print(mytool.info_text)
        box = layout.box()
        box = box.column(align=True) # reduces line separation in text field
        for line in mytool.info_text.split('\n'):
            box.label(text=line.strip())
        #layout.prop(mytool, "info_text", icon='VIEWZOOM')

# ------------------------------------------------------------------------
#    Registration
# ------------------------------------------------------------------------

classes = (
    MyProperties,
    WM_OT_AddMolecule,
    WM_OT_AddUnitCell,
    WM_OT_AddSurface,
    WM_OT_AddOrbital,
    WM_OT_AddVDWSurface,
    WM_OT_AddESPSurface,
    WM_OT_AddModes,
    #OBJECT_MT_CustomMenu,
    OBJECT_PT_MoleculePanel
)

def register():
    from bpy.utils import register_class
    for cls in classes:
        register_class(cls)

    bpy.types.Scene.my_tool = PointerProperty(type=MyProperties)

def unregister():
    from bpy.utils import unregister_class
    for cls in reversed(classes):
        unregister_class(cls)
    del bpy.types.Scene.my_tool


if __name__ == "__main__":
    register()

