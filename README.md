# Blender Add-On: blmol

This code allows to visualize chemical structures and  data of cube files using the Blender software.
It is based on the [blmol project of Scott Hartley](https://github.com/scotthartley/blmol),
with additional features including:

* Molecular structures from xyz, pdb, cube, log and molden files
* Isosurfaces from cube files
* Crystal structures and unit cells from cif files
* Normal mode vectors from Gaussian log files
* Simple usage from within the Blender UI

For full documentation and installation instructions, see [doc/blender.md](https://gitlab.com/tricht0rk/blmol/-/blob/master/doc/blender.md).

<img src="doc/pic/phe-LUMO.png" width=200>
<img src="doc/pic/phe-vh.png" width=200>
<img src="doc/pic/modes-sticks.png" width=200>