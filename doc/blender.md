# Blender Add-On: blmol

## Introduction

This site explains how to visualize chemical structures and data using the Blender software.
It is based on the [blmol project of Scott Hartley](https://github.com/scotthartley/blmol) (forked in 2018),
but heavily modified to work as an add-on within in the Blender UI, support more file formats and include various additional plotting capabilities.
Features include:

* Molecular structures from xyz, pdb, cube, log and molden files
* Isosurfaces from cube files
* Crystal structures and unit cells from cif files
* Normal mode vectors from Gaussian log files
* Simple usage from within the Blender UI

You can obtain all necessary files from the gitlab repo:
[https://gitlab.com/tricht0rk/blmol](https://gitlab.com/tricht0rk/blmol)

This document is organized as follows:

* **Installation:** Explains how to install the add-on and customize Blender in order to process cube files.
* **Tutorial:** Explains how to get molecular structures and orbital surfaces from xyz and cube files.
* **Manual:** Explains add-on functionality in more detail.
* **Further Steps:** Some ideas to further customize Blender output.

## Installation

### Download and install Blender

Download the current version of [Blender](https://www.blender.org/)
As of 07.10.2020, the scripts are tested with version 2.90.
Uncompress the downloaded file to some local directory.
Blender should run out-of-the-box without compilation by changing into the new directory and typing in the console:

```bash
./blender
```

The directory in which Blender was installed is from now on called `$BLENDER`.

### Install local python libraries for Blender

In order to process cube files several additional python modules have to be installed. This can be done in two ways; either with the help of an add-on or manually.

#### Installation via PIP add-on

Download the Blender PIP add-on:

[https://github.com/amb/blender_pip](https://github.com/amb/blender_pip)

In Blender navigate to Edit &#8594; Preferences &#8594; Add-Ons. Click on "Install", navigate to the downloaded zip-archive and hit "Install Addon".

You should now see the Python Module Manager add-on. To activate the local PIP version click "Ensure PIP" and "Upgrade PIP".

Now type the following package names one by one in the pip_module_name text field and click "Install" for every package:

```
wheel
PyMCubes
scipy
pillow
```

If everything works without error messages you can proceed to the installation of the blmol add-on. If you encounter problems, select or deselect the box called "As local user" and try again. If you still get errors, try the manual installation process described below.

#### Manual installation
Blender comes with an own python installation.
Therefore, make sure to install the packages not to your local installation, but to the one that comes packed with Blender.
In the following, the Blender version is 2.90 and the python version is 3.7.7.
In future releases it might be necessary, to adjust the paths accordingly.

Change to the local python directory:

```
cd $BLENDER/2.90/python/bin
```
Make sure the the pip package manager is used and up-to-date:

```bash
./python3.7m -m ensurepip
./python3.7m -m pip install -U pip
```
The next steps require gcc. If you do not have it installed, you can get it e.g. on Ubuntu via:

```bash
sudo apt-get install build-essential 
```

In order to compile c code with python bindings, the local blender installation requires several header files. A hacky way to include these is, to simply create softlinks from any other python installation to the include directory of your local blender installation. It works for me, if I link to the include files of my local anaconda installation. It is important, that the python version is the same as used in Blender.

```bash
cd $BLENDER/2.90/python/include/python3.7m
cp -s -r $PYTHONDIRECTORY/include/python3.7/* .
```

Install the packages wheel, PyMCubes (necessary for the marching cubes algorithm), scipy, and pillow (necessary for output of png-images).:

```bash
./pip install wheel
./pip install --upgrade PyMCubes
./pip install --upgrade scipy
./pip install pillow
```

### Install the blmol module

In Blender navigate to Edit &#8594; Preferences &#8594; Add-Ons. Click on "Install" and navigate to "src/blmol_addon_001.py" and hit "Install Addon". The plugin appears under the name "Add Mesh: Blender for Molecules". Make sure that it is activated.

In the 3D view in object mode you should now see a new tab called "Molecule" (below the tabs "Item", "View" and "Tool").

## Tutorial

This tutorial assumes that you are familiar with the basic usage of Blender.
Some useful resources to learn the basics are the [Blender wikibook](https://en.wikibooks.org/wiki/Blender_3D:_Noob_to_Pro), [Blender documentation](https://docs.blender.org/manual/en/latest/) and [Blender forum](https://blenderartists.org/)

Start Blender, open `src/initial_setup.blend` and save it under a new name.

<img src="pic/phe-blender.png" width=800>

You should see a screen similar to the image above (except for the molecule).
On the left there are two 3D views, on the right the scene collection and properties.
The yellow box marks the blmol add-on interface.
In the center of the camera view will be an empty object called "CameraFocus".
Use this object to point the camera in different directions.
Because the add-on renders molecular structures and surfaces as individual objects it is recommended to not move the created objects, but instead move the camera.
Otherwise, orbitals/surfaces might not correctly align with molecular structures.

In the path file dialog select `tutorial/phe.xyz` and hit "Draw Molecule". Now you should see the pictured phenylalanine.
In order to see the colors of atoms inside Blender, change viewport shading to "Material Preview" or "Rendered". If the molecule seems to contain way too many bonds, change "File Unit" to Angstrom. If there are still problems with bonds (too few or too many), try to adjust the "Bond Guess Threshold". Changes of this parameters will not affect existing objects. Every time, "Draw Molecule" is pressed, a new object with the current parameter settings will be created.

Pressing F12 renders the image in a new window.

<img src="pic/phe-geom.png">

By selecting cube files with different content, surfaces of e.g. electron densities or orbital wave functions can be drawn.

Select the file `tutorial/phe-LUMO.cube` and hit "Draw Orbital".
The threshold value at which the surfaces are drawn can be adjusted right below the "Draw Orbital" button. Two surfaces, one for positive and one for negative threshold value will be drawn.

<img src="pic/phe-LUMO.png">

Select the file `tutorial/phe-density.cube` and hit "Draw Surface".
Now only a single surface at the choosen threshold value will be drawn.
By clicking on the camera/eye icons in the very right of the Blender window, the different objects can be hidden during rendering and in the interface.

<img src="pic/phe-density.png">

It is possible to color every vertex of a surface separately using data stored in a cube file.
To do this, hit "Draw Colored Surface".

**Caution: This might take some time, since the add-on has to interpolate color values for every vertex of the surface!**

A surface at the van-der-Waals distance around the molecule's atoms is drawn and colored according to the values stored in the selected cube file.
Drawing a colored surface of the electron density of phenylalanine at the van-der-Waals distance, will yield the picture below.

<img src="pic/phe-density-vdw.png">

Crystal structures can be created from cif files. Select the file `tutorial/ZnO.cif` and click on "Draw Molecule" and "Draw Unit Cell". For crystals you might need to increase the bond guess threshold (0.25 nm works fine for this tutorial). You can control the number of repetitions along the lattice vectors using the "A/B/C Repetitions" sliders.

<img src="pic/ZnO-structure.png">

Normal mode vectors can be read from Gaussian log files. Select the file `tutorial/modes.log` and change the structure type to "Sticks" for better visibility of the normal mode vectors. Then click "Draw Molecule".
The info text at the bottom of the user interface should state, that the add-on found 10 atoms and 24 normal modes.
Use the "Normal Mode" input to select a mode and hit "Draw Modes".
The normal mode vectors will be added as green arrows.
Lenght and radius can be adjusted using "Scale" and "Radius" input. Negative scaling values will invert the direction of arrows.
Pictured below is normal mode 4 of the tutorial file.

<img src="pic/modes-sticks.png">

## Manual

This manual explains the different settings of the blmol add-on interface:

<img src="pic/interface.png">

### Input Data

General parameters

* **Path** File selection dialog
* **File Units** Units of loaded file. Either Angstrom or bohr (a.u.).

### Structure Operations

These operations are available for xyz, pdb, cube, cif and molden files.

* **Structure** Type of structure to draw. "Balls and Sticks", "Spheres" or "Sticks"
* **Draw Molecule** Draw the molecule object.
* **Draw Hydrogen** Select if hydrogen atoms shall be drawn.
* **Bond Guess Threshold** xyz and cube files do not contain information of molecular bonds. Existence of bonds (important for structure type "Balls and Sticks" and "Sticks") is guessed based on distance between atoms. Unit is nm.
* **Sphere Resolution** Resolution of drawn spheres. Increase to get higher quality structures (also increases render time!).
* **A/B/C Repetitions** Control the number of unit cell repetitions along lattice vectors a, b and c, respectively. Only available for cif files.
* **Draw Unit Cell** Draw the crystal unit cell. Only available for cif files.
* **Draw Modes** Draw normal mode vector. Only available for log files.
* **Normal Mode** Index of normal mode to draw.
* **Scale** Scaling factor for normal mode vector arrows. Negative scaling factors will invert direction of arrows.
* **Radius** Radius (thickness) of normal mode vector arrows.
* **Surface/Orbital Resolution** Resolution of van-der-Waals surfaces.
* **Draw vdW Surface** Draw a surface at the van-der-Waals distance around the molecule. Quality can be controlled using "Surface/Orbital Resolution".

### Volume Operations

These operations are only available for cube files.

* **Draw Colored Surface** Draw a van-der-Waals surface with resolution determined by the loaded cube file. Color each vertex according to the data stored in the cube file. **Caution: Very slow!**
* **Draw Surface** Draw a single surface at the given threshold value.
* **Draw Orbital** Draw two surfaces, one at positive and one at negative threshold value.
* **Threshold** Threshold for drawing of surfaces.

Other parameters listed in section "Molden Operations" (Orbital Index, MO, Diffuse, MO Threshold) are related to the molden file format and are highly experimental. Do not use unless you are familiar with the code.

At the bottom of the interface a text field with information about the loaded file is shown.

## Further Steps

Having structures and surfaces obtained, the full power of Blender can be harnessed to improve material and render quality.

`src/materials.blend` comes with a few procedural materials that can be easily imported and applied (see below).
For volumetric materials and glass shaders, it is recommended to switch to Cycles render engine.

<img src="pic/materials.png" width=500>
<img src="pic/materials_cycles.png" width=500>
